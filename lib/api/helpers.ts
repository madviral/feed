import axiosConfig from 'src/settings/axios'

const GET_REQUEST = async (url: string, params: {} = {}, headers: {} = {}) => {
  try {
    const response = await axiosConfig({
      url,
      method: 'GET',
      params,
      headers,
    })
    if (response.status === 200) {
      return {
        ...response,
        success: true,
      }
    } else {
      return {
        ...response,
        success: false,
      }
    }
  } catch (error) {
    // TODO: Добавить логирование ошибок апи-запросов
    // tslint:disable-next-line:no-console
    return error
  }
}

const POST_REQUEST = async (url: string, data: {} = {}, headers: {} = {}) => {
  try {
    const response = await axiosConfig({
      url,
      method: 'POST',
      data,
      headers,
    })
    if (response.status === 200) {
      return {
        ...response,
        success: true,
      }
    } else {
      return {
        ...response,
        success: false,
      }
    }
  } catch (error) {
    // TODO: Добавить логирование ошибок апи-запросов
    if (error?.response?.status < 500) {
      return {
        ...error?.response,
        success: false,
      }
    } else {
      throw new Error(error)
    }
  }
}

const PUT_REQUEST = async (url: string, data: {} = {}, headers: {} = {}) => {
  try {
    const response = await axiosConfig({
      url,
      method: 'PUT',
      data,
      headers,
    })
    return response
  } catch (error) {
    // TODO: Добавить логирование ошибок апи-запросов
    // tslint:disable-next-line:no-console
    console.error(error)
  }
}

const DELETE_REQUEST = async (url: string, headers: {} = {}) => {
  try {
    const response = await axiosConfig({
      url,
      method: 'DELETE',
      headers,
    })
    return response
  } catch (error) {
    // TODO: Добавить логирование ошибок апи-запросов
    // tslint:disable-next-line:no-console
    console.error(error)
  }
}

export { GET_REQUEST, POST_REQUEST, PUT_REQUEST, DELETE_REQUEST }
