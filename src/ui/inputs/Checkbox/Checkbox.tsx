import React, { FC } from 'react'
import { CheckboxProps } from './interfaces'
import styled from 'ui/theme/styled'

const FormCheckbox = styled.div`
  input {
    position: absolute;
    display: none;

    & + label {
      position: relative;
      font-size: 14px;
      line-height: 18px;
      color: ${props => props.theme.colorList.secondary[0]};
    }

    & + label:before {
      content: '';
      margin-right: 26px;
      display: inline-block;
      vertical-align: text-top;
      width: 18px;
      height: 18px;
      border: 2px solid ${props => props.theme.colorList.secondary[0]};
      border-radius: ${props => props.theme.borderRadius.xxs};
      box-sizing: border-box;
      background-color: transparent;
      transform: translateY(-1px);
    }

    &:checked + label:before {
      border: none;
      background-color: ${props => props.theme.colorList.primary[0]};
    }

    &:checked + label:after {
      content: '';
      position: absolute;
      top: 1px;
      left: 7px;
      width: 3px;
      height: 9px;
      border-bottom: 2px solid ${props => props.theme.colorList.secondary[9]};
      border-right: 2px solid ${props => props.theme.colorList.secondary[9]};
      transform: rotate(45deg);
    }
  }
`

const Checkbox: FC<CheckboxProps> = ({ id, value, name, label, onChange }) => {
  return (
    <FormCheckbox>
      <input
        type='checkbox'
        id={id}
        value={value}
        name={name}
        onChange={event => onChange(event.target.value, id)}
      />
      <label htmlFor={id}>{label}</label>
    </FormCheckbox>
  )
}

export default Checkbox
