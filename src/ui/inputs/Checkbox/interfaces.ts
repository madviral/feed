export interface CheckboxProps {
  id: string
  name: string
  label: string
  value: string | number
  onChange?: (value: string, id: string) => void
}
