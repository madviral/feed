import React, { createRef, FC, useEffect, useState } from 'react'
import { SearchInputProps } from './types'
import { useStyles } from './styles'
import {
  LoupeIcon,
  CloseIcon,
  useEffectAfterMount,
} from '@vlife-grand-era/vlife-storybook'

const SearchInput: FC<SearchInputProps> = ({
  placeholder,
  onSearchClose,
  searchIsOpen,
  onSearchTextChange,
}) => {
  const classes = useStyles({ searchIsOpen })
  const [searchText, setSearchText] = useState('')

  useEffectAfterMount(() => {
    onSearchTextChange(searchText)
  }, [searchText])

  const handleInput = event => {
    setSearchText(event.target.value)
  }

  const handleClearAndSearch = () => {
    if (!searchText) {
      onSearchClose()
    } else {
      setSearchText('')
    }
  }

  return (
    <div className={classes.searchInputWrapper}>
      <LoupeIcon className={classes.searchLoupe} />
      <input
        className={classes.searchInput}
        type='text'
        id='search-input'
        placeholder={placeholder}
        onChange={event => handleInput(event)}
        value={searchText}
      />
      <button
        className={classes.searchCloseButton}
        onClick={handleClearAndSearch}
      >
        <CloseIcon />
      </button>
    </div>
  )
}

SearchInput.defaultProps = {
  placeholder: 'Поиск',
}

export default SearchInput
