export interface SearchProps {
  onOpenValueChange?: (value: boolean) => void
  onSearchTextChange: (searchText: string) => void
}

export interface SearchInputProps {
  placeholder?: string
  onSearchClose: () => void
  searchIsOpen?: boolean
  onSearchTextChange: (searchText: string) => void
}
