import { createUseStyles } from 'react-jss'

export const useStyles = createUseStyles({
  searchMainWrapper: {
    width: '100%',
    display: 'flex',
    position: 'relative',
  },
  closedSearchButton: ({ searchIsOpen }) => ({
    display: !searchIsOpen ? 'flex' : 'none',
    alignItems: 'center',
    justifyContent: 'center',
    width: 48,
    minHeight: 48,
    maxHeight: 48,
    position: 'absolute',
    top: 0,
    right: 0,
    background: 'none',
    cursor: 'pointer',
    border: 'none',
    padding: 0,
    transition: 'all .15s ease-in-out',
    zIndex: 1,

    '&:focus': {
      outline: 'unset',
    },
  }),
  searchInputWrapper: ({ searchIsOpen }) => ({
    width: '100%',
    maxWidth: '100%',
    minHeight: 48,
    maxHeight: 48,
    padding: '0 8px',
    position: 'relative',
    display: 'flex',
    transition: searchIsOpen ? 'all .15s ease-in' : 'none',
    opacity: searchIsOpen ? 1 : 0,
  }),
  searchInput: {
    width: '100%',
    minHeight: 48,
    maxHeight: 48,
    padding: 0,
    paddingLeft: 58,
    fontSize: 17,
    fontFamily: 'Roboto',
    lineHeight: '20px',
    border: 'none',
    borderRadius: 24,

    '&::placeholder': {
      color: '#CCCCCC',
      fontFamily: 'Roboto',
    },

    '&:focus': {
      outline: 'none',
    },
  },
  searchLoupe: {
    position: 'absolute',
    transform: 'translateY(-50%)',
    top: '50%',
    left: 20,

    '& path': {
      fill: '#000000',
    },
  },
  searchCloseButton: {
    width: 24,
    minHeight: 24,
    maxHeight: 24,
    position: 'absolute',
    transform: 'translateY(-50%)',
    top: '50%',
    right: 20,
    background: 'none',
    border: 'none',
    padding: 0,
    cursor: 'pointer',

    '&:focus': {
      outline: 'unset',
    },
  },
})
