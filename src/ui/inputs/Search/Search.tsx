import React, { FC, useEffect, useState } from 'react'
import SearchInput from './SearchInput'
import { SearchProps } from './types'
import { useStyles } from './styles'
import { LoupeIcon } from '@vlife-grand-era/vlife-storybook'

const Search: FC<SearchProps> = ({ onOpenValueChange, onSearchTextChange }) => {
  const [searchIsOpen, setSearchIsOpen] = useState(false)
  const classes = useStyles({ searchIsOpen })

  if (onOpenValueChange) {
    useEffect(() => {
      onOpenValueChange(searchIsOpen)
    }, [searchIsOpen])
  }

  const handleCloseSearch = () => {
    setSearchIsOpen(false)
  }

  return (
    <div className={classes.searchMainWrapper}>
      <button
        className={classes.closedSearchButton}
        onClick={() => setSearchIsOpen(true)}
      >
        <LoupeIcon />
      </button>
      <SearchInput
        searchIsOpen={searchIsOpen}
        onSearchClose={handleCloseSearch}
        onSearchTextChange={onSearchTextChange}
      />
    </div>
  )
}

export default Search
