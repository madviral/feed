export interface TextAreaProps {
  id: string
  value?: string
  placeholder: string
  onChange?: (value?: string) => void
}
