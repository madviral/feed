import React, { FC } from 'react'
import { styled } from 'ui/theme'
import { TextAreaProps } from './interfaces'

const TextAreaInput = styled.textarea`
  width: 100%;
  height: 300px;
  padding: 16px;
  background-color: ${props => props.theme.colorList.secondary[7]};
  box-sizing: border-box;
  outline: none;
  border: none;
  resize: none;
`

const TextArea: FC<TextAreaProps> = ({ id, value, placeholder, onChange }) => {
  return (
    <TextAreaInput
      id={id}
      value={value}
      placeholder={placeholder}
      onChange={event => onChange(event.target.value)}
    />
  )
}

export default TextArea
