export interface FormFieldProps {
  id: string
  type?: string
  value?: string
  label?: string
  placeholder: string
  borderRadius?: string
  onChange?: (value?: string) => void
  onEnter?: (value?: string) => void
}
