import React, { FC } from 'react'
import { FormFieldProps } from './interfaces'
import { styled } from 'ui/theme'

const FormLabel = styled.label`
  display: block;
  margin-bottom: 4px;
  font-size: 12px;
  line-height: 16px;
  color: #808080;
`

const FormInput = styled.input`
  width: 100%;
  padding: 14px 16px;
  border-radius: 4px 4px 0px 0px;
  background-color: ${props => props.theme.colorList.secondary[7]};
  box-sizing: border-box;
  color: ${props => props.theme.colorList.secondary[0]};
  outline: none;
  border: none;
`

const FormField: FC<FormFieldProps> = ({
  id,
  type,
  value,
  label,
  placeholder,
  onChange,
  onEnter,
}) => {
  return (
    <>
      <FormLabel htmlFor={id}>{label}</FormLabel>
      <FormInput
        id={id}
        type={type}
        value={value}
        placeholder={placeholder}
        autoComplete='off'
        onChange={event => onChange(event.target.value)}
        onKeyPress={e => {
          if (e.key === 'Enter') {
            onEnter((e.target as HTMLInputElement).value)
          }
        }}
      />
    </>
  )
}

FormField.defaultProps = {
  type: 'text',
  onChange: () => {
    return
  },
  onEnter: () => {
    return
  },
}

export default FormField
