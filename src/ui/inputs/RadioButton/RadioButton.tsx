import React, { FC } from 'react'
import { RadioButtonProps, RadioButtonStyleProps } from './interfaces'
import styled from 'ui/theme/styled'

const FormRadioButton = styled.div<RadioButtonStyleProps>`
  position: relative;

  &:not(:last-child) {
    margin-bottom: ${props => props.gap};
  }

  input {
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 0;
    opacity: 0;
    visibility: hidden;
  }

  input[type='radio'] + label {
    position: relative;
    display: block;
    padding: ${props => props.pd};
    font-size: ${props => props.fontSize};
    line-height: ${props => props.lineHeight};
    font-weight: ${props => props.fontWeight};
    background-color: ${props => props.bgColor};
    color: ${props => props.fontWeight};
  }

  input[type='radio'] + label:after {
    display: none;
    content: url(/images/checked.svg);
    position: absolute;
    top: 12px;
    right: 16px;
  }

  input[type='radio']:checked + label:after {
    display: block;
  }
`

const RadioButton: FC<RadioButtonProps & RadioButtonStyleProps> = ({
  id,
  value,
  name,
  label,
  checked,
  pd,
  gap,
  fontSize,
  lineHeight,
  fontWeight,
  bgColor,
  color,
  onChange,
}) => (
  <FormRadioButton
    pd={pd}
    color={color}
    bgColor={bgColor}
    fontSize={fontSize}
    lineHeight={lineHeight}
    fontWeight={fontWeight}
    gap={gap}
  >
    <input
      type='radio'
      id={id}
      name={name}
      value={value}
      checked={checked}
      onChange={event => onChange(event.target.value, id)}
    />
    <label htmlFor={id}>{label}</label>
  </FormRadioButton>
)

FormRadioButton.defaultProps = {
  pd: '14px 16px',
  gap: '1px',
  fontSize: '16px',
  lineHeight: '20px',
  color: '#212121',
  bgColor: '#fff',
}

export default RadioButton
