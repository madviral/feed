export interface RadioButtonProps {
  id: string
  name: string
  label: string
  value: string | number
  checked?: boolean
  onChange: (value: string, id?: string) => void
}

export interface RadioButtonStyleProps {
  pd?: string
  color?: string
  bgColor?: string
  fontSize?: string
  lineHeight?: string
  fontWeight?: number
  gap?: string
}
