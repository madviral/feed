import { ReactNode } from 'react'

export interface RoundedButtonProps {
  icon: ReactNode
  transparent?: boolean
  onButtonClick?: () => void
}

export interface ButtonProps {
  transparent: boolean
}
