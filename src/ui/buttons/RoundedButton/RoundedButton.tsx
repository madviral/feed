import React, { FC } from 'react'
import { styled } from 'ui'
import { ButtonProps, RoundedButtonProps } from './props'

const Button = styled.button<ButtonProps>`
  display: flex;
  padding: 8px;
  border-radius: ${props => props.theme.borderRadius.r};
  background-color: ${props => props.theme.colorTools.getSecondary(5, 0.2)};
  ${props => (props.transparent ? `background-color: transparent` : ``)};
  border: none;
  outline: none;
`

const RoundedButton: FC<RoundedButtonProps> = ({
  icon,
  transparent,
  onButtonClick,
}) => (
  <Button transparent={transparent} onClick={onButtonClick}>
    {icon}
  </Button>
)

export default RoundedButton
