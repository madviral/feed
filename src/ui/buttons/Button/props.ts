import { ReactNode } from 'react'

export interface ButtonProps {
  text: string
  pd?: string
  borderRadius: string
  bgColor?: string
  width?: string
  fullWidth?: boolean
  color?: string
  fontSize?: string
  lineHeight?: string
  fontWeight?: number
  icon?: ReactNode
  onClick?: () => void
}

export interface GeneralButtonProps {
  pd: string
  bgColor: string
  borderRadius: string
  width: string
  fullWidth: boolean
}

export interface ButtonText {
  color: string
  fontSize: string
  lineHeight?: string
  fontWeight?: number
}
