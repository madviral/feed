import React, { FC } from 'react'
import { Text, styled } from 'ui'
import { ButtonProps, GeneralButtonProps, ButtonText } from './props'
import { Box } from 'ui/layout'

const GeneralButton = styled.button<GeneralButtonProps>`
  display: flex;
  align-items: center;
  justify-content: center;
  width: ${props => (props.fullWidth ? '100%' : props.width)};
  padding: ${props => props.pd};
  border-radius: ${props => props.borderRadius};
  background-color: ${props => props.bgColor};
  border: none;
  outline: none;
`

const ButtonText = styled.span<ButtonText>`
  font-size: ${props => props.fontSize};
  line-height ${props => props.lineHeight};
  font-weight: ${props => props.fontWeight};
  color: ${props => props.color};
`

const Button: FC<ButtonProps> = ({
  text,
  pd,
  color,
  fontSize,
  lineHeight,
  fontWeight,
  bgColor,
  borderRadius,
  width,
  fullWidth,
  icon,
  onClick,
}) => (
  <GeneralButton
    pd={pd}
    bgColor={bgColor}
    borderRadius={borderRadius}
    width={width}
    fullWidth={fullWidth}
    onClick={() => onClick()}
  >
    {icon && <Box mr='8px'>{icon}</Box>}
    <Text
      color={color}
      fontSize={fontSize}
      lineHeight={lineHeight}
      fontWeight={fontWeight}
    >
      {text}
    </Text>
  </GeneralButton>
)

Button.defaultProps = {
  pd: '8px',
  color: '#fff',
  fontSize: '12px',
  fontWeight: 500,
  fullWidth: false,
}

export default Button
