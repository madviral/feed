import {
  flexbox,
  grid,
  space,
  layout,
  position,
  border,
  color,
  shadow,
} from 'styled-system'
import styled from 'ui/theme/styled'

const Box = styled.div`
  box-sizing: border-box;
  ${layout}
  ${position}
  ${grid}
  ${flexbox}
  ${space}
  ${border}
  ${color}
  ${shadow}
`
export default Box
