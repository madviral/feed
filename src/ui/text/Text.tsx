import { typography, space, color } from 'styled-system'
import { styled, theme } from '../theme'

const Text = styled.span`
  ${typography};
  ${space};
  ${color};
`

Text.defaultProps = {
  theme,
}

export default Text
