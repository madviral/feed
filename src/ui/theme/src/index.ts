import fontWeight from './fontWeight'
import fontFamily from './fontFamily'
import lineHeight from './lineHeight'
import borderRadius from './borderRadius'
import fontSize from './fontSize'
import colorList, { colorTools } from './color'

export const theme = {
  fontSize,
  fontWeight,
  fontFamily,
  lineHeight,
  borderRadius,
  colorList,
  colorTools,
}

export * from './types'
