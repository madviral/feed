export interface ThemeFontSize {
  xs: string
  s: string
  m: string
  l: string
  xl: string
}

export interface ThemeBorderRadius {
  xxs: string
  xs: string
  s: string
  m: string
  l: string
  xl: string
  r: string
}

export interface ThemeFontFamily {
  ms: string
}

export interface ThemeLineHeight {
  xs: number
  s: number
  m: number
  l: number
  xl: number
}

export interface ThemeFontWeight {
  light: number
  regular: number
  medium: number
  semibold: number
  bold: number
  black: number
}

export interface ThemeColors {
  [color: string]: string | string[]
}

export interface ThemeColorTools {
  getPrimary: (index: number, alpha: number) => string
  getSecondary: (index: number, alpha: number) => string
}

export interface Theme {
  fontWeight: ThemeFontWeight
  fontFamily: ThemeFontFamily
  lineHeight: ThemeLineHeight
  borderRadius: ThemeBorderRadius
  colorList: ThemeColors
  colorTools: ThemeColorTools
}
