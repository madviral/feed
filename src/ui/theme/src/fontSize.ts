import { ThemeFontSize } from './types'

const fontSize: ThemeFontSize = {
  xs: '10px',
  s: '12px',
  m: '14px',
  l: '16px',
  xl: '18px',
}

export default fontSize
