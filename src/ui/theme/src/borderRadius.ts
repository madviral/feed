import { ThemeBorderRadius } from './types'

const borderRadius: ThemeBorderRadius = {
  xxs: '4px',
  xs: '8px',
  s: '10px',
  m: '16px',
  l: '20px',
  xl: '24px',
  r: '50%',
}

export default borderRadius
