import { ThemeColorTools, ThemeColors } from './types'

interface RGB {
  r: number
  g: number
  b: number
}

type HexToRgb = (hex: string | string[]) => RGB | null
type GetRGBa = (color: RGB, alpha?: number) => string

const hexToRgb: HexToRgb = hex => {
  if (typeof hex !== 'string') {
    return null
  }
  const result = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i.exec(hex)
  return result
    ? {
        r: parseInt(result[1], 16),
        g: parseInt(result[2], 16),
        b: parseInt(result[3], 16),
      }
    : null
}

const getRGBa: GetRGBa = ({ r, g, b }, alpha = 1) =>
  `rgba(${r}, ${g}, ${b}, ${alpha})`

export const colorList: ThemeColors = {
  primary: [
    '#0027BC',
    '#003CD4',
    '#0047DF',
    '#0252EC',
    '#0F5BF8', // 4
    '#4D77FB',
    '#7490FC',
    '#A0B0FD',
    '#C7CFFD',
    '#E9ECFF', // 9
  ],
  secondary: [
    '#212121',
    '#424242',
    '#616161',
    '#757575',
    '#9E9E9E', // 4
    '#BDBDBD',
    '#E0E0E0',
    '#EEEEEE',
    '#F5F5F5',
    '#FFFFFF', // 9
  ],
  fern: [
    '#1B5E20',
    '#2E7D32',
    '##388E3C',
    '#43A047',
    '#4CAF50', // 4
    '#66BB6A',
    '#81C784',
    '#A5D6A7',
    '#C8E6C9',
    '#E8F5E9', // 9
  ],
}

export const colorTools: ThemeColorTools = {
  getPrimary: (index, alpha) => {
    return getRGBa(hexToRgb(colorList.primary[index]), alpha)
  },
  getSecondary: (index, alpha) => {
    return getRGBa(hexToRgb(colorList.secondary[index]), alpha)
  },
}

export default colorList
