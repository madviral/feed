export { default as styled } from './styled'
export { default as ThemeProvider } from './ThemeProvider'
export { ThemeProvider as EmotionThemeProvider } from './EmotionThemeProvider'

export { theme } from './src'
