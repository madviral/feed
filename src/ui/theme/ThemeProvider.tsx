import React from 'react'
import { ThemeProvider as JssThemeProvider } from 'react-jss'

const ThemeProvider = ({ theme, children }) => (
  <JssThemeProvider theme={theme}>{children}</JssThemeProvider>
)

export default ThemeProvider
