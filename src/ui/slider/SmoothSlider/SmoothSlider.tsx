import React, { FC } from 'react'
import { styled } from 'ui'
import { SliderProps } from '../props'
import { SliderItem } from '../SliderItem'
import { ResourceItem } from 'src/common'

const SliderWrapper = styled.form`
  display: flex;
  align-items: center;
  overflow-x: scroll;
  overflow-y: hidden;
  white-space: nowrap;

  -webkit-overflow-scrolling: touch;

  &::-webkit-scrollbar {
    display: none;
  }
`

const SmoothSlider: FC<SliderProps> = ({
  list,
  name,
  selectedItem,
  onSelect,
}) => (
  <SliderWrapper>
    <SliderItem
      id='0'
      name={name}
      value='all'
      content='Все'
      pd='13px 12px'
      checked={selectedItem === null || selectedItem === 'all'}
      onClick={value => onSelect(value)}
    />
    {list.map((item: ResourceItem) => (
      <SliderItem
        key={item.id}
        id={item.id.toString()}
        name={name}
        value={item.value}
        icon={item.icon}
        content={item.name}
        checked={selectedItem === item.value}
        onClick={value => onSelect(value)}
      />
    ))}
  </SliderWrapper>
)

export default SmoothSlider
