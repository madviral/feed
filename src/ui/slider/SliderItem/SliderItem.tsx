import React, { FC } from 'react'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { styled } from 'ui'
import { ItemProps, ItemLayoutProps } from '../props'

const Item = styled.div<ItemLayoutProps>`
  position: relative;

  input {
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    height: 0;
    opacity: 0;
    visibility: hidden;
  }

  input[type='radio'] + label {
    display: flex;
    align-items: center;
    padding: ${props => props.pd};
    background-color: ${props => props.theme.colorList.secondary[9]};
    border-radius: ${props => props.theme.borderRadius.xl};

    img {
      width: 32px;
      height: 32px;
      margin-right: 4px;
    }

    span {
      font-size: ${props => props.fontSize};
      line-height: ${props => props.lineHeight};
      font-weight: ${props => props.fontWeight};
      color: ${props => props.color};
    }
  }

  input[type='radio']:checked + label {
    background-color: ${props => props.theme.colorList.secondary[1]};
    color: ${props => props.theme.colorList.secondary[9]};
  }

  &:not(:last-child) {
    margin-right: ${props => props.gap};
  }
`

const SliderItem: FC<ItemProps> = ({
  id,
  name,
  value,
  icon,
  content,
  checked,
  color,
  pd,
  gap,
  fontSize,
  lineHeight,
  fontWeight,
  onClick,
}) => (
  <Item
    pd={pd}
    color={color}
    fontSize={fontSize}
    lineHeight={lineHeight}
    fontWeight={fontWeight}
    gap={gap}
  >
    <input
      type='radio'
      id={value}
      name={name}
      value={value}
      checked={checked}
      onChange={e => onClick(e.target.value)}
    />
    <label htmlFor={value}>
      <Condition match={icon}>
        <img src={icon} alt={name} />
      </Condition>
      <span> {content} </span>
    </label>
  </Item>
)

Item.defaultProps = {
  pd: '4px 12px 4px 4px',
  gap: '4px',
  fontSize: '12px',
  lineHeight: '14px',
  fontWeight: 600,
  color: '#4D77FB',
}

export default SliderItem
