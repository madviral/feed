import { ResourceItem } from 'src/common'

export interface SliderProps {
  list: ResourceItem[]
  name: string
  selectedItem: string
  onSelect: (value: string) => void
}

export interface ItemProps {
  id: string
  name: string
  value: string
  icon?: string
  content: any
  checked?: boolean
  color?: string
  fontSize?: string
  lineHeight?: string
  fontWeight?: number
  pd?: string
  gap?: string
  onClick: (value: string) => void
}

export interface ItemLayoutProps {
  color: string
  fontSize: string
  lineHeight: string
  fontWeight: number
  pd: string
  gap: string
}
