import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Refresh: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M6.35828 6.35097C7.98828 4.72097 10.2983 3.78097 12.8383 4.04097C16.5083 4.41097 19.5283 7.39097 19.9383 11.061C20.4883 15.911 16.7383 20.001 12.0083 20.001C8.81828 20.001 6.07828 18.131 4.79828 15.441C4.47828 14.771 4.95828 14.001 5.69828 14.001C6.06828 14.001 6.41828 14.201 6.57828 14.531C7.70828 16.961 10.4183 18.501 13.3783 17.841C15.5983 17.351 17.3883 15.541 17.8583 13.321C18.6983 9.44097 15.7483 6.00097 12.0083 6.00097C10.3483 6.00097 8.86828 6.69097 7.78828 7.78097L9.29828 9.29097C9.92828 9.92097 9.48828 11.001 8.59828 11.001H5.00828C4.45828 11.001 4.00828 10.551 4.00828 10.001V6.41097C4.00828 5.52097 5.08828 5.07097 5.71828 5.70097L6.35828 6.35097Z'
      fill={color}
    />
  </Icon>
)

Refresh.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#bdbdbd',
}

export default Refresh
