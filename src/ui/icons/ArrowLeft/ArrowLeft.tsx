import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const ArrowLeft: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M19 11.0007H7.82998L12.71 6.1207C13.1 5.7307 13.1 5.0907 12.71 4.7007C12.32 4.3107 11.69 4.3107 11.3 4.7007L4.70998 11.2907C4.31998 11.6807 4.31998 12.3107 4.70998 12.7007L11.3 19.2907C11.69 19.6807 12.32 19.6807 12.71 19.2907C13.1 18.9007 13.1 18.2707 12.71 17.8807L7.82998 13.0007H19C19.55 13.0007 20 12.5507 20 12.0007C20 11.4507 19.55 11.0007 19 11.0007Z'
      fill={color}
    />
  </Icon>
)

ArrowLeft.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#4D77FB',
}

export default ArrowLeft
