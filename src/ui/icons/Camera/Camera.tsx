import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Camera: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M23.3333 5.83333H19.6349L18.1883 4.25833C17.7566 3.78 17.1266 3.5 16.4733 3.5H11.5266C10.8733 3.5 10.2433 3.78 9.79992 4.25833L8.36492 5.83333H4.66659C3.38325 5.83333 2.33325 6.88333 2.33325 8.16667V22.1667C2.33325 23.45 3.38325 24.5 4.66659 24.5H23.3333C24.6166 24.5 25.6666 23.45 25.6666 22.1667V8.16667C25.6666 6.88333 24.6166 5.83333 23.3333 5.83333ZM13.9999 11.6667C12.0669 11.6667 10.4999 13.2337 10.4999 15.1667C10.4999 17.0997 12.0669 18.6667 13.9999 18.6667C15.9329 18.6667 17.4999 17.0997 17.4999 15.1667C17.4999 13.2337 15.9329 11.6667 13.9999 11.6667ZM8.16658 15.1667C8.16658 18.3867 10.7799 21 13.9999 21C17.2199 21 19.8333 18.3867 19.8333 15.1667C19.8333 11.9467 17.2199 9.33333 13.9999 9.33333C10.7799 9.33333 8.16658 11.9467 8.16658 15.1667Z'
      fill={color}
    />
  </Icon>
)

Camera.defaultProps = {
  originalWidth: 28,
  originalHeight: 28,
  width: 28,
  height: 28,
  color: '#BDBDBD',
}

export default Camera
