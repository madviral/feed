import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Share: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M13.5 9.00003V16C13.5 16.5523 13.0523 17 12.5 17C11.9477 17 11.5 16.5523 11.5 16V9.00003H5.5C4.94772 9.00003 4.5 9.44774 4.5 10V22C4.5 22.5523 4.94772 23 5.5 23H19.5C20.0523 23 20.5 22.5523 20.5 22V10C20.5 9.44774 20.0523 9.00003 19.5 9.00003H13.5Z'
      fill={color}
    />
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M13.2071 1.29292L12.5 0.585815L11.7929 1.29292L7.79289 5.29292C7.40237 5.68345 7.40237 6.31661 7.79289 6.70714C8.18342 7.09766 8.81658 7.09766 9.20711 6.70714L11.5 4.41424V9.00003H13.5V4.41424L15.7929 6.70714C16.1834 7.09766 16.8166 7.09766 17.2071 6.70714C17.5976 6.31661 17.5976 5.68345 17.2071 5.29292L13.2071 1.29292Z'
      fill={color}
    />
  </Icon>
)

Share.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#9E9E9E',
}

export default Share
