import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Category: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M4 8C4.55228 8 5 7.55228 5 7C5 6.44772 4.55228 6 4 6C3.44772 6 3 6.44772 3 7C3 7.55228 3.44772 8 4 8ZM4 13C4.55228 13 5 12.5523 5 12C5 11.4477 4.55228 11 4 11C3.44772 11 3 11.4477 3 12C3 12.5523 3.44772 13 4 13ZM5 17C5 17.5523 4.55228 18 4 18C3.44772 18 3 17.5523 3 17C3 16.4477 3.44772 16 4 16C4.55228 16 5 16.4477 5 17ZM8 6C7.44772 6 7 6.44772 7 7C7 7.55228 7.44772 8 8 8H19C19.5523 8 20 7.55228 20 7C20 6.44772 19.5523 6 19 6H8ZM7 12C7 11.4477 7.44772 11 8 11H19C19.5523 11 20 11.4477 20 12C20 12.5523 19.5523 13 19 13H8C7.44772 13 7 12.5523 7 12ZM8 16C7.44772 16 7 16.4477 7 17C7 17.5523 7.44772 18 8 18H19C19.5523 18 20 17.5523 20 17C20 16.4477 19.5523 16 19 16H8Z'
      fill={color}
    />
  </Icon>
)

Category.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#212121',
}

export default Category
