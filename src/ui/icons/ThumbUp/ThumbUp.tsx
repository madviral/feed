import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const ThumbUp: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M7.9091 8.29186L13.4491 2.75186C14.0291 2.16186 14.9691 2.16186 15.5591 2.74186C15.9191 3.10186 16.0691 3.61186 15.9691 4.11186L15.0191 8.69186H20.6691C22.8191 8.69186 24.2691 10.8919 23.4291 12.8719L20.1691 20.4819C19.8491 21.2119 19.1291 21.6919 18.3291 21.6919H9.3291C8.2291 21.6919 7.3291 20.7919 7.3291 19.6919V9.70186C7.3291 9.17186 7.5391 8.66186 7.9091 8.29186ZM5.3291 19.6919C5.3291 20.7919 4.4291 21.6919 3.3291 21.6919C2.2291 21.6919 1.3291 20.7919 1.3291 19.6919V11.6919C1.3291 10.5919 2.2291 9.69186 3.3291 9.69186C4.4291 9.69186 5.3291 10.5919 5.3291 11.6919V19.6919Z'
      fill={color}
    />
  </Icon>
)

ThumbUp.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#9E9E9E',
}

export default ThumbUp
