import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Caution: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M31.1498 6H17.8698C17.3498 6 16.8298 6.22 16.4698 6.58L7.08977 15.96C6.72977 16.32 6.50977 16.84 6.50977 17.36V30.62C6.50977 31.16 6.72977 31.66 7.08977 32.04L16.4498 41.4C16.8298 41.78 17.3498 42 17.8698 42H31.1298C31.6698 42 32.1698 41.78 32.5498 41.42L41.9098 32.06C42.2898 31.68 42.4898 31.18 42.4898 30.64V17.36C42.4898 16.82 42.2698 16.32 41.9098 15.94L32.5498 6.58C32.1898 6.22 31.6698 6 31.1498 6ZM24.5098 34.6C23.0698 34.6 21.9098 33.44 21.9098 32C21.9098 30.56 23.0698 29.4 24.5098 29.4C25.9498 29.4 27.1098 30.56 27.1098 32C27.1098 33.44 25.9498 34.6 24.5098 34.6ZM22.5098 24C22.5098 25.1 23.4098 26 24.5098 26C25.6098 26 26.5098 25.1 26.5098 24V16C26.5098 14.9 25.6098 14 24.5098 14C23.4098 14 22.5098 14.9 22.5098 16V24Z'
      fill={color}
    />
  </Icon>
)

Caution.defaultProps = {
  originalWidth: 48,
  originalHeight: 48,
  width: 48,
  height: 48,
  color: '#808080',
}

export default Caution
