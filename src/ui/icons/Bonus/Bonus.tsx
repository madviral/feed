import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Bonus: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M2 12C2 6.48 6.48 2 12 2C17.52 2 22 6.48 22 12C22 17.52 17.52 22 12 22C6.48 22 2 17.52 2 12ZM8.25 6.75C8.25 6.33579 8.58579 6 9 6H14.5C14.9142 6 15.25 6.33579 15.25 6.75C15.25 7.16421 14.9142 7.5 14.5 7.5H9C8.58579 7.5 8.25 7.16421 8.25 6.75ZM8.25 10C8.25 9.44771 8.69771 9 9.25 9L14.5 8.99998C14.9142 8.99998 15.25 9.33577 15.25 9.74998C15.25 10.1642 14.9142 10.5 14.5 10.5L9.75 10.5V12.5H13H13.75C14.8546 12.5 15.75 13.3954 15.75 14.5V15.25V16C15.75 17.1046 14.8546 18 13.75 18H11.75H9.25C8.69772 18 8.25 17.5523 8.25 17V10ZM9.75 16.5V14H12H13.25C13.8023 14 14.25 14.4477 14.25 15V15.25V15.5C14.25 16.0523 13.8023 16.5 13.25 16.5H12H9.75Z'
      fill={color}
    />
  </Icon>
)

Bonus.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#43A047',
}

export default Bonus
