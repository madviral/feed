import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Pencil: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M20.7085 5.63126C21.0985 6.02127 21.0985 6.65127 20.7085 7.04127L18.8785 8.87126L15.1285 5.12126L16.9585 3.29126C17.1454 3.10401 17.399 2.99878 17.6635 2.99878C17.9281 2.99878 18.1817 3.10401 18.3685 3.29126L20.7085 5.63126ZM2.99854 20.5013V17.4613C2.99854 17.3213 3.04854 17.2013 3.14854 17.1013L14.0585 6.19126L17.8085 9.94127L6.88854 20.8513C6.79854 20.9513 6.66854 21.0013 6.53854 21.0013H3.49854C3.21854 21.0013 2.99854 20.7813 2.99854 20.5013Z'
      fill={color}
    />
  </Icon>
)

Pencil.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#ffffff',
}

export default Pencil
