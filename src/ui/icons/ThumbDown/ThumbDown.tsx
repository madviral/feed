import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const ThumbDown: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M17.0784 15.7081L11.5484 21.2481C10.9684 21.8381 10.0284 21.8381 9.43836 21.2581C9.07836 20.8981 8.92836 20.3881 9.02836 19.8881L9.97836 15.3081H4.32836C2.17836 15.3081 0.728357 13.1081 1.57836 11.1281L4.83836 3.51811C5.14836 2.78811 5.86836 2.30811 6.66836 2.30811H15.6584C16.7584 2.30811 17.6584 3.20811 17.6584 4.30811V14.2981C17.6584 14.8281 17.4484 15.3381 17.0784 15.7081ZM19.6684 4.30811C19.6684 3.20811 20.5684 2.30811 21.6684 2.30811C22.7684 2.30811 23.6684 3.20811 23.6684 4.30811V12.3081C23.6684 13.4081 22.7684 14.3081 21.6684 14.3081C20.5684 14.3081 19.6684 13.4081 19.6684 12.3081V4.30811Z'
      fill={color}
    />
  </Icon>
)

ThumbDown.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#9E9E9E',
}

export default ThumbDown
