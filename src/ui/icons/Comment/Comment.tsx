import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Comment: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M22.5 4C22.5 2.9 21.6 2 20.5 2H4.5C3.4 2 2.5 2.9 2.5 4V16C2.5 17.1 3.4 18 4.5 18H18.5L22.5 22V4Z'
      fill={color}
    />
  </Icon>
)

Comment.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#9E9E9E',
}

export default Comment
