import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const Image: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      fillRule='evenodd'
      clipRule='evenodd'
      d='M28 6.66667V25.3333C28 26.8 26.8 28 25.3333 28H6.66667C5.2 28 4 26.8 4 25.3333V6.66667C4 5.2 5.2 4 6.66667 4H25.3333C26.8 4 28 5.2 28 6.66667ZM14.6667 22.0133L11.8667 18.64C11.5867 18.3067 11.08 18.32 10.8267 18.6667L7.50667 22.9333C7.16 23.3733 7.46667 24.0133 8.02667 24.0133H24.0133C24.56 24.0133 24.88 23.3867 24.5467 22.9467L19.8667 16.7067C19.6 16.3467 19.0667 16.3467 18.8 16.6933L14.6667 22.0133Z'
      fill={color}
    />
  </Icon>
)

Image.defaultProps = {
  originalWidth: 32,
  originalHeight: 32,
  width: 32,
  height: 32,
  color: '#E0E0E0',
}

export default Image
