export interface IconProps {
  originalWidth?: number
  originalHeight?: number
  width?: number
  height?: number
  fill?: string
  stroke?: string
  viewBox?: string
  color?: string
  className?: string
}
