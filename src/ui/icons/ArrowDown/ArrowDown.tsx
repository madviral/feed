import React, { FC } from 'react'
import { IconProps, Icon } from '../Icon'

const ArrowDown: FC<IconProps> = ({
  originalWidth,
  originalHeight,
  color,
  ...props
}) => (
  <Icon
    originalHeight={originalHeight}
    originalWidth={originalWidth}
    {...props}
  >
    <path
      d='M8.12002 9.29006L12 13.1701L15.88 9.29006C16.27 8.90006 16.9 8.90006 17.29 9.29006C17.68 9.68006 17.68 10.3101 17.29 10.7001L12.7 15.2901C12.31 15.6801 11.68 15.6801 11.29 15.2901L6.70002 10.7001C6.31002 10.3101 6.31002 9.68006 6.70002 9.29006C7.09002 8.91006 7.73002 8.90006 8.12002 9.29006Z'
      fill={color}
    />
  </Icon>
)

ArrowDown.defaultProps = {
  originalWidth: 24,
  originalHeight: 24,
  width: 24,
  height: 24,
  color: '#CCCCCC',
}

export default ArrowDown
