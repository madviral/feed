import React, { FC } from 'react'
import { Box } from 'ui'

interface ConatinerProps {
  bgColor: string
}

const Container: FC<ConatinerProps> = ({ bgColor, children }) => (
  <Box position='relative' height='100%' backgroundColor={bgColor}>
    {children}
  </Box>
)

export default Container
