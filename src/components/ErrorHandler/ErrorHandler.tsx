import React, { FC, ReactNode } from 'react'
import { Box, Text, theme, Button, CautionIcon } from 'ui'
interface ErrorHandlerProps {
  icon?: ReactNode
  title?: string
  text?: string
  buttonText?: string
  onButtonClick?: () => void
}

const ErrorHandler: FC<ErrorHandlerProps> = ({
  icon,
  title,
  text,
  buttonText,
  onButtonClick,
}) => (
  <Box
    display='flex'
    flexDirection='column'
    alignItems='center'
    justifyContent='center'
    p={32}
    backgroundColor={theme.colorList.secondary[9]}
    borderRadius={theme.borderRadius.m}
  >
    <Box mb={16}>{icon}</Box>
    <Text
      as='p'
      fontWeight={theme.fontWeight.semibold}
      fontSize={20}
      lineHeight='24px'
      letterSpacing='-0.24px'
      color={theme.colorList.secondary[1]}
      textAlign='center'
    >
      {title}
    </Text>
    <Text
      as='p'
      mb={16}
      fontSize={12}
      lineHeight='16px'
      color={theme.colorList.secondary[3]}
      textAlign='center'
    >
      {text}
    </Text>
    <Button
      width='218px'
      borderRadius={theme.borderRadius.m}
      bgColor={theme.colorList.primary[5]}
      text={buttonText}
      onClick={() => onButtonClick()}
    />
  </Box>
)

ErrorHandler.defaultProps = {
  icon: <CautionIcon />,
  title: 'Загрузка не завершена',
  text: 'Произошла ошибка перехода либо прервано соединение с интернетом',
  buttonText: 'Перезагрузить',
}

export default ErrorHandler
