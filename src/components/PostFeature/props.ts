export interface PostFeatureProps {
  postId: number
  likes: number
  disLikes: number
  vlikes?: number
  thumbState: number
  thumbRequest?: ({}: { postId: number; thumbType: number }) => void
}
