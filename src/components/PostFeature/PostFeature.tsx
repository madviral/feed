import React, { FC, useState } from 'react'
import { Dispatch, bindActionCreators } from 'redux'
import { connect } from 'react-redux'
import { Drawer } from '@material-ui/core'
import {
  Box,
  Text,
  theme,
  VlikeIcon,
  ThumbUpIcon,
  ThumbDownIcon,
  ShareIcon,
  styled,
  CrossRoundedIcon,
  BonusIcon,
  FormField,
  Button,
} from 'ui'
import * as thumbActions from 'src/common/actions/rating'
import { PostFeatureProps } from './props'

const FeatureButton = styled.button`
  display: flex;
  flex-direction: column;
  align-items: center;
  padding: 0;
  background-color: transparent;
  outline: none;
  border: none;
`

const ButtonClose = styled.button`
  display: flex;
  background-color: transparent;
  padding: 0;
  border: none;
  outline: none;
`

const PostFeature: FC<PostFeatureProps> = ({
  postId,
  likes,
  disLikes,
  thumbState,
  thumbRequest,
}) => {
  const [thumbStatus, setThumbStatus] = useState(thumbState)
  const [thumbUp, setThumbUp] = useState(likes)
  const [thumbDown, setThumbDown] = useState(disLikes)
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)

  const handleThumbUpClick = () => {
    if (thumbStatus === 1) {
      thumbRequest({ postId, thumbType: 1 })
      setThumbUp(thumbUp - 1)
      setThumbStatus(0)
    } else if (thumbStatus === 2) {
      thumbRequest({ postId, thumbType: 1 })
      setThumbDown(thumbDown - 1)
      setThumbUp(thumbUp + 1)
      setThumbStatus(1)
    } else {
      thumbRequest({ postId, thumbType: 1 })
      setThumbUp(thumbUp + 1)
      setThumbStatus(1)
    }
  }

  const handleThumbDownClick = () => {
    if (thumbStatus === 1) {
      thumbRequest({ postId, thumbType: 2 })
      setThumbUp(thumbUp - 1)
      setThumbDown(thumbDown + 1)
      setThumbStatus(2)
    } else if (thumbStatus === 2) {
      thumbRequest({ postId, thumbType: 2 })
      setThumbDown(thumbDown - 1)
      setThumbStatus(0)
    } else {
      thumbRequest({ postId, thumbType: 2 })
      setThumbDown(thumbDown + 1)
      setThumbStatus(2)
    }
  }

  return (
    <>
      <Box display='flex' alignItems='flex-end' justifyContent='space-between'>
        <FeatureButton onClick={() => setIsDrawerOpen(true)}>
          <VlikeIcon />
          <Text
            fontSize='10px'
            lineHeight='12px'
            color={theme.colorList.secondary[4]}
          >
            0
          </Text>
        </FeatureButton>
        <FeatureButton onClick={handleThumbUpClick}>
          {thumbStatus === 1 ? (
            <ThumbUpIcon color={theme.colorList.secondary[0]} />
          ) : (
            <ThumbUpIcon />
          )}
          <Text
            fontSize='10px'
            lineHeight='12px'
            color={theme.colorList.secondary[4]}
          >
            {thumbUp}
          </Text>
        </FeatureButton>
        <FeatureButton onClick={handleThumbDownClick}>
          {thumbStatus === 2 ? (
            <ThumbDownIcon color={theme.colorList.secondary[0]} />
          ) : (
            <ThumbDownIcon />
          )}
          <Text
            fontSize='10px'
            lineHeight='12px'
            color={theme.colorList.secondary[4]}
          >
            {thumbDown}
          </Text>
        </FeatureButton>
        <FeatureButton>
          <ShareIcon />
          <Text
            fontSize='10px'
            lineHeight='12px'
            color={theme.colorList.secondary[4]}
          >
            Поделиться
          </Text>
        </FeatureButton>
      </Box>
      <Drawer
        classes={{ paper: 'custom_drawer' }}
        anchor='bottom'
        open={isDrawerOpen}
        onClose={() => setIsDrawerOpen(false)}
      >
        <Box p={16}>
          <Box
            mb='8px'
            display='flex'
            alignItems='center'
            justifyContent='space-between'
          >
            <Text
              fontWeight={theme.fontWeight.semibold}
              fontSize='20px'
              lineHeight='24px'
              letterSpacing='-0.24px'
              color={theme.colorList.secondary[1]}
            >
              VЛайк
            </Text>
            <ButtonClose onClick={() => setIsDrawerOpen(false)}>
              <CrossRoundedIcon />
            </ButtonClose>
          </Box>
          <Text
            as='p'
            fontSize='12px'
            lineHeight='16px'
            color={theme.colorList.secondary[3]}
          >
            Вы можете поддержать автора понравившейся публикации бонусами.
            Введите сумму, которую согласны перевести.
          </Text>
          <Box
            display='flex'
            alignItems='center'
            justifyContent='space-between'
            py={16}
            mt={16}
            mb={16}
            boxShadow='inset 0px -0.5px 0px #E5E5E5, inset 0px 0.5px 0px #E5E5E5'
          >
            <Text
              as='p'
              fontSize='16px'
              lineHeight='18px'
              letterSpacing='-0.24px'
              color={theme.colorList.secondary[0]}
            >
              Мои бонусы
            </Text>
            <Box display='flex' alignItems='center'>
              <Text
                mr='6px'
                fontWeight={theme.fontWeight.semibold}
                fontSize='20px'
                lineHeight='24px'
                letterSpacing='-0.24px'
                color={theme.colorList.fern[3]}
              >
                12 543
              </Text>
              <BonusIcon />
            </Box>
          </Box>
          <FormField id='1' type='tel' placeholder='Введите сумму' />
          <Box mt={16}>
            <Button
              text='Поддержать'
              borderRadius='8px'
              fullWidth
              pd='12px 16px'
            />
          </Box>
        </Box>
      </Drawer>
    </>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(thumbActions, dispatch),
})

export default connect(null, mapDispatchToProps)(PostFeature)
