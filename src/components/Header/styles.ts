import styled from 'src/ui/theme/styled'

import { HeaderWrapProps } from './interfaces'

export const HeaderWrap = styled.div<HeaderWrapProps>`
  display: flex;
  min-height: 40px;
  align-items: center;

  ${props => `
    padding-top: 16px;
    padding-bottom: 16px;
    padding-left: ${props.searchIsOpen ? '0' : '16px'};
    padding-right: ${props.searchIsOpen ? '0' : '16px'};
    transition: ${
      props.searchIsOpen ? 'all .15s ease-in' : 'all .15s ease-out'
    };
    background: ${
      props.searchIsOpen ? props.theme.colorList.primary[5] : 'transparent'
    };
  `};

  .__left {
    flex: 2 1 100%;
    align-items: center;
    display: ${props => (props.searchIsOpen ? 'none' : 'flex')};
  }

  .__back-button {
    margin-right: 16px;
    min-width: 40px;
  }

  .__header-desc {
    color: ${props => props.theme.colorList.secondary[5]};
    font-size: 12px;
  }

  .__right {
    flex: 1 2 100%;
    display: flex;
    justify-content: flex-end;
    align-items: center;
  }

  .__avatar {
    display: ${props => props.searchIsOpen && 'none'};
    min-width: 40px;
    max-width: 40px;
    min-height: 40px;
    max-height: 40px;
    border: 2px solid #ffffff;
    border-radius: 50%;
    box-sizing: border-box;
    box-shadow: 0px 1px 4px rgba(0, 0, 0, 0.2);
    margin-left: 16px;
    object-fit: cover;
  }
`
