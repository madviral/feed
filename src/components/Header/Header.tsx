import React, { FC, useState } from 'react'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { HeaderWrap } from './styles'
import { HeaderProps } from './interfaces'
import { theme, Text, Search, RoundedButton, ArrowLeftIcon } from 'ui'

const Header: FC<HeaderProps> = ({
  title,
  hasBackButton,
  withPhoto,
  displayRight,
  fontWeight,
  fontSize,
  letterSpacing,
  color,
  onSearchTextChange,
  onProfileClick,
  onBackButtonClick,
}) => {
  const [searchIsOpen, setSearchIsOpen] = useState(false)

  return (
    <HeaderWrap searchIsOpen={searchIsOpen}>
      <div className='__left'>
        <Condition match={hasBackButton}>
          <RoundedButton
            icon={<ArrowLeftIcon />}
            onButtonClick={onBackButtonClick}
          />
        </Condition>
        <Condition match={title}>
          <Text
            ml={16}
            fontWeight={fontWeight}
            fontSize={fontSize}
            letterSpacing={letterSpacing}
            color={color}
          >
            {title}
          </Text>
        </Condition>
      </div>
      <Condition match={displayRight}>
        <div className='__right'>
          <Condition match={Boolean(onSearchTextChange)}>
            <Search
              onSearchTextChange={onSearchTextChange}
              onOpenValueChange={setSearchIsOpen}
            />
          </Condition>
          <Condition match={withPhoto}>
            <img
              className='__avatar'
              alt='profilePhoto'
              src={'https://i.ibb.co/brjtnbt/images.png'}
              onClick={onProfileClick}
            />
          </Condition>
        </div>
      </Condition>
    </HeaderWrap>
  )
}

Header.defaultProps = {
  hasBackButton: true,
  displayRight: true,
  fontWeight: theme.fontWeight.bold,
  fontSize: '20px',
  letterSpacing: '-0.24px',
  color: theme.colorList.secondary[1],
}

export default Header
