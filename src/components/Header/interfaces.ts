export interface HeaderProps {
  title?: string
  withPhoto?: boolean
  hasBackButton?: boolean
  displayRight?: boolean
  fontWeight?: number
  fontSize?: string
  letterSpacing?: string
  color?: string
  onSearchTextChange?: (searchText: string) => void
  onProfileClick?: () => void
  onBackButtonClick?: () => void
}

export interface HeaderWrapProps {
  searchIsOpen: boolean
}
