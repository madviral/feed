import React, { useEffect, FC, Suspense } from 'react'
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { jss } from 'react-jss'
import vendorPrefixer from 'jss-plugin-vendor-prefixer'
import { GuardProvider, GuardedRoute } from 'react-router-guards'
import * as authActions from '../src/common/actions/auth'
import { history } from './common/history'
import { onNativeBackPressed } from './common/utils'
import { routes } from './routes'
import { TokenObj, AUTH_ONLY } from './common'

jss.use(vendorPrefixer())

const App = ({ authObj, requestAuth }) => {
  const someToken = {
    accessToken:
      'eyJhbGciOiJSUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICJKdXJOdGcycGxuTUlYSlprcmJKQVVKbEI5bHYxN1QwOUJONVNzTk9yd2xRIn0.eyJqdGkiOiJkNTQ0OTYzNS1kNjYyLTQ5MWYtYTQ4NS05YTEzNDRmOTU0Y2IiLCJleHAiOjE1OTU4NjA0NDMsIm5iZiI6MCwiaWF0IjoxNTk1ODM4ODQzLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4zLjQ3OjgwODAvYXV0aC9yZWFsbXMvU3ByaW5nQm9vdFRlc3QiLCJhdWQiOlsicmVhbG0tbWFuYWdlbWVudCIsImFjY291bnQiXSwic3ViIjoiMGI0N2IxYjktNDg4MS00ZjI1LTk2ZmItYmJmZjA5ZjVkYmRhIiwidHlwIjoiQmVhcmVyIiwiYXpwIjoibG9naW4tYXBwIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiMWEyNzg3MzQtMDk1Mi00Yzg0LWEzM2MtZjUzNTU3MDhlNzdkIiwiYWNyIjoiMSIsInJlYWxtX2FjY2VzcyI6eyJyb2xlcyI6WyJvZmZsaW5lX2FjY2VzcyIsImNsaWVudCIsInVtYV9hdXRob3JpemF0aW9uIl19LCJyZXNvdXJjZV9hY2Nlc3MiOnsicmVhbG0tbWFuYWdlbWVudCI6eyJyb2xlcyI6WyJ2aWV3LXJlYWxtIiwibWFuYWdlLXVzZXJzIiwidmlldy11c2VycyIsInZpZXctY2xpZW50cyIsInF1ZXJ5LWNsaWVudHMiLCJxdWVyeS1ncm91cHMiLCJxdWVyeS11c2VycyJdfSwiYWNjb3VudCI6eyJyb2xlcyI6WyJtYW5hZ2UtYWNjb3VudCIsIm1hbmFnZS1hY2NvdW50LWxpbmtzIiwidmlldy1wcm9maWxlIl19fSwic2NvcGUiOiJlbWFpbCBwcm9maWxlIiwiZW1haWxfdmVyaWZpZWQiOmZhbHNlLCJwcmVmZXJyZWRfdXNlcm5hbWUiOiI3NzQ3NTI2NjU0MyJ9.l3fiIGJUaZUO7aPAgbKcOBCDMcj2xVNvN1BX2wKTG_Ln9eCGZSIVZ4USjWTc9iuOXpri0_Cva5pQwxZ7g9qszla7o9t4cnS1klM1xoilBpvQkZAOHkKuAHiAmYkH91FqZMq7c9ir3SzdBnVY0LuyR7GGQAqwrWRAbTszWgdh8aNyh7ak_GlV2a34-8TW0RBtxOEQWONCgYgMvHYPeyd-1I8YPHIjvm9Pst8qlmPFpllLqMgUP3dMEKxtZCibq7sepFaWf7GY15bqYOegRHzllI7YfPYP-87-6l1pFfWG1j_Qr3V-oMbdMjuWqBplmbHGFTPbHyD88bsOwUNMdD8-yg',
    refreshToken:
      'eyJhbGciOiJIUzI1NiIsInR5cCIgOiAiSldUIiwia2lkIiA6ICIzZDFmYTJjZC1hYzVlLTQ0NGEtODRkMC1iZjBmNjFhY2JkOTEifQ.eyJqdGkiOiI0Y2M3ODU0NS1jNTFiLTRlYjAtOTIzMy01MTdiNzA1ZmRjZmYiLCJleHAiOjE1OTYwNTk2MzIsIm5iZiI6MCwiaWF0IjoxNTk2MDMwODMyLCJpc3MiOiJodHRwOi8vMTkyLjE2OC4zLjQ3OjgwODAvYXV0aC9yZWFsbXMvU3ByaW5nQm9vdFRlc3QiLCJhdWQiOiJodHRwOi8vMTkyLjE2OC4zLjQ3OjgwODAvYXV0aC9yZWFsbXMvU3ByaW5nQm9vdFRlc3QiLCJzdWIiOiIwYjQ3YjFiOS00ODgxLTRmMjUtOTZmYi1iYmZmMDlmNWRiZGEiLCJ0eXAiOiJSZWZyZXNoIiwiYXpwIjoibG9naW4tYXBwIiwiYXV0aF90aW1lIjowLCJzZXNzaW9uX3N0YXRlIjoiZDI0ZGYzZmYtYTg1Ni00YzdmLWEzZjItMzY4ZDAwZjhhYzZiIiwicmVhbG1fYWNjZXNzIjp7InJvbGVzIjpbIm9mZmxpbmVfYWNjZXNzIiwiY2xpZW50IiwidW1hX2F1dGhvcml6YXRpb24iXX0sInJlc291cmNlX2FjY2VzcyI6eyJyZWFsbS1tYW5hZ2VtZW50Ijp7InJvbGVzIjpbInZpZXctcmVhbG0iLCJtYW5hZ2UtdXNlcnMiLCJ2aWV3LXVzZXJzIiwidmlldy1jbGllbnRzIiwicXVlcnktY2xpZW50cyIsInF1ZXJ5LWdyb3VwcyIsInF1ZXJ5LXVzZXJzIl19LCJhY2NvdW50Ijp7InJvbGVzIjpbIm1hbmFnZS1hY2NvdW50IiwibWFuYWdlLWFjY291bnQtbGlua3MiLCJ2aWV3LXByb2ZpbGUiXX19LCJzY29wZSI6ImVtYWlsIHByb2ZpbGUifQ.8MzQeUQfRN3B1EzatSOm2koxjI3XOf8QX4tMagolqXY',
    locale: 'ru',
    isAuthenticated: true,
  }

  useEffect(() => {
    // requestAuth(someToken)
    if (window) {
      window.tokenInit = tokenInit
      window.onBackPressed = onBackPressed
    }
  }, [])

  const requireAuth = (to, from, next) => {
    if (to.meta[AUTH_ONLY] && !authObj.isAuthenticated) {
      next.redirect('./')
    }
    next()
  }

  const tokenInit = (props: TokenObj) => {
    requestAuth(props)
  }

  const onBackPressed = () => onNativeBackPressed(history)

  return (
    <Router>
      <GuardProvider guards={[requireAuth]}>
        <Suspense fallback={<div></div>}>
          <Switch>
            {routes.map(route => (
              <Route
                key={route.key}
                path={route.path}
                exact={route.exact}
                component={route.component}
                meta={route.meta}
              />
            ))}
          </Switch>
        </Suspense>
      </GuardProvider>
    </Router>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(authActions, dispatch),
})

const mapStateToProps = state => ({
  authObj: state.auth,
})

export default connect(mapStateToProps, mapDispatchToProps)(App)
