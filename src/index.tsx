import React from 'react'
import * as ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { EmotionThemeProvider, ThemeProvider, theme } from 'ui'
import 'assets/main.css'
import App from './App'
import { store } from './store/configureStore/configureStore'

declare global {
  interface Window {
    tokenInit: any
    onBackPressed: any
  }

  const Android: any
}

const Index = () => (
  <Provider store={store}>
    <EmotionThemeProvider>
      <ThemeProvider theme={theme}>
        <App />
      </ThemeProvider>
    </EmotionThemeProvider>
  </Provider>
)

ReactDOM.render(<Index />, document.getElementById('root'))
