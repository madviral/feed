import React from 'react'
import { Feed } from './modules'
import { AUTH_ONLY } from './common/constants'

const CreatePost = React.lazy(() =>
  import('./modules/createPost/pages/CreatePost/CreatePost'),
)

const Profile = React.lazy(() =>
  import('./modules/profile/pages/Profile/Profile'),
)

const PostDetail = React.lazy(() =>
  import('./modules/feed/pages/PostDetail/PostDetail'),
)

export const routes = [
  {
    path: '/',
    exact: true,
    key: 'main',
    component: Feed,
    meta: {
      [AUTH_ONLY]: false,
    },
  },
  {
    path: '/post/:postId',
    exact: true,
    key: 'detail',
    component: PostDetail,
    meta: {
      [AUTH_ONLY]: false,
    },
  },
  {
    path: '/create',
    exact: true,
    key: 'create',
    component: CreatePost,
    meta: {
      [AUTH_ONLY]: true,
    },
  },
  {
    path: '/profile/:userId?',
    exact: true,
    key: 'profile',
    component: Profile,
    meta: {
      [AUTH_ONLY]: true,
    },
  },
]
