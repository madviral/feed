export const onNativeBackPressed = history => {
  if (history.location.pathname === '/' && typeof Android !== 'undefined') {
    Android.closeWebView()
  } else {
    history.goBack()
  }
}

export function withAuth(history, redirectUrl, isLoggedIn): Promise<any> {
  if (isLoggedIn) {
    return history.push(redirectUrl)
  } else {
    return Promise.resolve(true)
  }
}
