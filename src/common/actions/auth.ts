import { ActionCreator } from 'redux'
import * as actions from '../constants'
import { TokenObj } from '../types'

export const requestAuth = (token: TokenObj) => {
  return dispatch => dispatch(auth(token))
}

export const auth: ActionCreator<any> = (token: TokenObj) => ({
  type: actions.AUTH,
  token,
})
