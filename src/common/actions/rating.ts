import { AppThunk } from 'src/store/rootReducer'

export const thumbRequest = (body): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  const { error, data } = await client.post(`entertainment/feed/v1/thumb`, body)
  if (error) {
    alert(error)
  }
}
