import { AUTH } from '../constants'
import { TokenObj } from '../types'

const initialState: TokenObj = {
  accessToken: '',
  locale: '',
  refreshToken: '',
  isAuthenticated: false,
}

export function auth(state = initialState, action): TokenObj {
  switch (action.type) {
    case AUTH:
      return {
        ...state,
        ...action.token,
      }

    default:
      return state
  }
}
