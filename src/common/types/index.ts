export interface TokenObj {
  accessToken: string
  refreshToken: string
  locale: string
  isAuthenticated?: boolean
}
export interface CategoryItem {
  id: number
  name: string
  value: string
}

export interface ResourceItem {
  id: number
  name: string
  icon: string
  resourceType: string
  value: string
}

export interface PostItem {
  id: number
  userId?: string | null
  firstName?: string | null
  lastName?: string | null
  title: string
  body: string
  url: string
  image: string
  resource: string
  resourceType: string
  tags: string[] | null
  category?: string
  likesCount?: number
  disLikesCount?: number
  thumbState?: number
  dateCreated?: null
  dateUpdated?: null
  icon?: string
  locale?: string
  role?: string
  status?: number
}
