import { createPostConstants } from '../constants'
import { PostItem } from 'src/common'

export interface CreatedPostState {
  item: PostItem
}

const initialState: CreatedPostState = {
  item: null,
}

export function createdPost(state = initialState, action): CreatedPostState {
  switch (action.type) {
    case createPostConstants.CREATE_POST_SUCCESS:
      return {
        ...state,
        item: action.payload,
      }

    default:
      return state
  }
}
