import React from 'react'
import { styled } from 'ui'

const Image = styled.img`
  display: block;
  height: 60px;
  width: 60px;
  border-radius: 8px;
`

const UploadedImage = ({ url }) => <Image src={url} />

export default UploadedImage
