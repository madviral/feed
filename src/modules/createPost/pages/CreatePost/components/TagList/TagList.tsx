import React from 'react'
import { styled, Box, Text, theme } from 'ui'

const Wrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
`

const TagList = ({ tags }) => (
  <Wrapper>
    {tags.map((tag: string) => (
      <Box
        key={tag}
        p='6px'
        m='8px'
        backgroundColor={theme.colorList.primary[9]}
        borderRadius={theme.borderRadius.xxs}
      >
        <Text
          as='p'
          fontWeight={theme.fontWeight.medium}
          fontSize={16}
          lineHeight='20px'
          color={theme.colorList.primary[5]}
        >
          {tag}
        </Text>
      </Box>
    ))}
  </Wrapper>
)

export default TagList
