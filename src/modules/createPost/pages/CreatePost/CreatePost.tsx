import React, { useState, useEffect } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { useHistory } from 'react-router-dom'
import { Drawer } from '@material-ui/core'
import { Container } from 'src/components'
import {
  Box,
  Text,
  theme,
  RoundedButton,
  ArrowLeftIcon,
  FormField,
  TextArea,
  CameraIcon,
  Button,
  ArrowDownIcon,
  RadioButton,
} from 'ui'
import * as fileUploadActions from '../../../feed/actions/fileUpload'
import * as categoriesActions from '../../../feed/actions/categories'
import * as postsActions from '../../../feed/actions/posts'
import { FileUploader } from '../../../feed/components'
import { UploadedImage, TagList } from './components'
import { Queries } from '../../../feed/types/enums'
import { CategoryItem } from 'src/common'

const CreatePost = ({
  file,
  categories,
  fileUpload,
  createPostRequest,
  requestCategories,
}) => {
  const history = useHistory()
  useEffect(() => {
    if (window) {
      window.onBackPressed = onBackPressed
    }
  }, [])

  const onBackPressed = () => {
    if (history.location.pathname === '/' && typeof Android !== 'undefined') {
      Android.closeWebView()
    } else {
      history.goBack()
    }
  }

  const [title, setTitle] = useState(null)
  const [body, setBody] = useState(null)
  const [tags, setTags] = useState([])
  const [category, setCategory] = useState({
    categoryName: null,
    categoryId: null,
  })
  const [isDrawerOpen, setIsDrawerOpen] = useState(false)

  const query = new URLSearchParams(location.search)
  const handleLocationBack = () => history.push('./')
  const fileUploadHandler = (data: File) => fileUpload(data)
  const nextPage = () => {
    query.set(Queries.STEP, 'second')
    history.replace({ ...history.location, search: query.toString() })
  }
  const createPost = () => {
    const data = {
      body,
      categoryId: category.categoryId,
      imageUrl: file.image,
      tag: tags,
      title,
    }
    createPostRequest(data)
  }

  return (
    <Container bgColor={theme.colorList.secondary[9]}>
      <Box p={16}>
        <Box display='flex' alignItems='center' mb={16}>
          <RoundedButton
            icon={<ArrowLeftIcon />}
            onButtonClick={handleLocationBack}
          />
          <Text
            ml={16}
            fontWeight={theme.fontWeight.bold}
            fontSize='20px'
            lineHeight='24px'
            color={theme.colorList.secondary[0]}
          >
            Новая публикация
          </Text>
        </Box>
        {query.get(Queries.STEP) === 'first' && (
          <>
            <Box mb='1px'>
              <FormField
                id='0'
                placeholder='Введите заголовок'
                onChange={value => setTitle(value)}
              />
            </Box>
            <TextArea
              id='0'
              placeholder='Введите основной текст'
              onChange={value => setBody(value)}
            />
            <Box position='absolute' bottom='0' left='0' width='100%'>
              {file.image !== null && (
                <Box
                  p={16}
                  backgroundColor={theme.colorList.secondary[9]}
                  borderRadius='16px 16px 0px 0px'
                  boxShadow='0px 0px 16px rgba(0, 0, 0, 0.05), 0px 0px 4px rgba(0, 0, 0, 0.1)'
                >
                  <UploadedImage url={file.image} />
                </Box>
              )}
              <Box
                display='flex'
                justifyContent='space-between'
                py='6px'
                px='8px'
                backgroundColor={theme.colorList.secondary[9]}
                borderTop='1px solid #E5E5E5'
              >
                <FileUploader
                  icon={<CameraIcon />}
                  onFileUpload={fileUploadHandler}
                />
                {title !== null && body !== null && (
                  <Button
                    text='Далее'
                    pd='8px 16px'
                    color={theme.colorList.primary[5]}
                    borderRadius={theme.borderRadius.xs}
                    bgColor={theme.colorList.primary[9]}
                    onClick={nextPage}
                  />
                )}
              </Box>
            </Box>
          </>
        )}
        {query.get(Queries.STEP) === 'second' && (
          <>
            <Box mb={16}>
              <Text
                as='p'
                mb='4px'
                fontSize='12px'
                lineHeight='16px'
                color='#808080'
              >
                Категория
              </Text>
              <Box
                display='flex'
                align-items='center'
                justifyContent='space-between'
                py={14}
                px={16}
                backgroundColor={theme.colorList.secondary[7]}
                borderRadius={theme.borderRadius.xxs}
                onClick={() => {
                  setIsDrawerOpen(true)
                  requestCategories()
                }}
              >
                {category.categoryName === null ? (
                  <Text
                    as='p'
                    fontWeight={theme.fontWeight.medium}
                    fontSize='17px'
                    lineHeight='20px'
                    color={theme.colorList.secondary[3]}
                  >
                    Выберите категорию
                  </Text>
                ) : (
                  <Text
                    as='p'
                    fontWeight={theme.fontWeight.medium}
                    fontSize='17px'
                    lineHeight='20px'
                    color={theme.colorList.secondary[0]}
                  >
                    {category.categoryName}
                  </Text>
                )}
                <ArrowDownIcon />
              </Box>
            </Box>
            <Box mb='8px'>
              <FormField
                id='2'
                label='Теги'
                placeholder='Введите от 1 до 5 тегов'
                onEnter={value => setTags([...tags, value])}
              />
            </Box>
            <TagList tags={tags} />
            <Box position='absolute' bottom='0' left='0' width='100%'>
              {tags.length && (
                <Box
                  display='flex'
                  justifyContent='flex-end'
                  py='6px'
                  px='8px'
                  backgroundColor={theme.colorList.secondary[9]}
                  borderTop='1px solid #E5E5E5'
                >
                  <Button
                    text='Отправить на модерацию'
                    pd='8px 16px'
                    color={theme.colorList.secondary[9]}
                    borderRadius={theme.borderRadius.xs}
                    bgColor={theme.colorList.primary[5]}
                    onClick={createPost}
                  />
                </Box>
              )}
            </Box>
          </>
        )}
      </Box>
      <Drawer
        classes={{ paper: 'custom_drawer' }}
        anchor='bottom'
        open={isDrawerOpen}
        onClose={() => setIsDrawerOpen(false)}
      >
        {categories.list.map((categoryItem: CategoryItem) => (
          <Box
            key={categoryItem.id}
            backgroundColor={theme.colorList.secondary[9]}
            py={12}
            px={16}
            mb='1px'
          >
            <RadioButton
              id={categoryItem.id.toString()}
              value={categoryItem.value}
              name='category'
              label={categoryItem.name}
              onChange={(value, id) => {
                setCategory(prevState => {
                  return {
                    ...prevState,
                    categoryName: value,
                    categoryId: id,
                  }
                })
                setIsDrawerOpen(false)
              }}
            />
          </Box>
        ))}
      </Drawer>
    </Container>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(fileUploadActions, dispatch),
  ...bindActionCreators(categoriesActions, dispatch),
  ...bindActionCreators(postsActions, dispatch),
})

const mapStateToProps = state => ({
  file: state.fileUpload,
  categories: state.categories,
})

export default connect(mapStateToProps, mapDispatchToProps)(CreatePost)
