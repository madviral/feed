import { ActionCreator } from 'redux'
import { AppThunk } from 'src/store/rootReducer'
import { PostUpload } from 'src/modules'
import { PostItem } from 'src/common'
import { createPostConstants } from '../constants'

export const createPostRequest = (postData: PostUpload): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  const { error, data } = await client.post(
    `entertainment/feed/v1/posts/createPost`,
    postData,
  )
  if (!error) {
    window.location.reload()
  } else {
    alert(error)
  }
}

export const createPostSucceeded: ActionCreator<any> = (payload: PostItem) => ({
  type: createPostConstants.CREATE_POST_SUCCESS,
  payload,
})
