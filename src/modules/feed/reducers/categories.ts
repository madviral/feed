import { filterConstants, categoriesConstants } from '../constants'
import { CategoryItem } from 'src/common'

export interface CategoryState {
  loaded: boolean
  list: CategoryItem[]
  error?: any
}

const initialState: CategoryState = {
  loaded: false,
  list: [],
}

export function categories(state = initialState, action): CategoryState {
  switch (action.type) {
    case categoriesConstants.GET_CATEGORIES_SUCCESS:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    case categoriesConstants.GET_CATEGORIES_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
