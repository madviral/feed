import { fileUploadConstants } from '../constants'

export interface FileState {
  loaded: boolean
  image: string
  error?: null
}

const initialState: FileState = {
  loaded: false,
  image: null,
}

export function fileUpload(state = initialState, action): FileState {
  switch (action.type) {
    case fileUploadConstants.FILE_UPLOADED_SUCCESS:
      return {
        ...state,
        loaded: true,
        image: action.payload,
      }

    case fileUploadConstants.FILE_UPLOADED_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
