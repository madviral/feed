import { postConstants } from '../constants'
import { PostItem } from 'src/common'

export interface PostsState {
  loaded: boolean
  list: PostItem[]
  error?: null
}

const initialState: PostsState = {
  loaded: false,
  list: [],
}

export function posts(state = initialState, action): PostsState {
  switch (action.type) {
    case postConstants.GET_POSTS:
      return {
        ...state,
        loaded: false,
      }

    case postConstants.GET_POSTS_SUCCESS:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    case postConstants.GET_POSTS_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
