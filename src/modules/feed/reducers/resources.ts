import { filterConstants } from '../constants'
import { ResourceItem } from 'src/common'

export interface ResourceState {
  loaded: boolean
  list: ResourceItem[]
  selectedRes?: string
}

const initialState: ResourceState = {
  loaded: false,
  list: [],
}

export function resources(state = initialState, action): ResourceState {
  switch (action.type) {
    case filterConstants.GET_RESOURCES:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    case filterConstants.SAVE_RESOURCE:
      return {
        ...state,
        selectedRes: action.payload,
      }

    case filterConstants.DELETE_RESOURCE:
      return {
        ...state,
        selectedRes: null,
      }

    default:
      return state
  }
}
