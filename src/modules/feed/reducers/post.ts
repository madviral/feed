import { postConstants } from '../constants'
import { PostItem } from 'src/common'

export interface PostState {
  loaded: boolean
  item: PostItem
  error?: null
}

const initialState: PostState = {
  loaded: false,
  item: null,
}

export function post(state = initialState, action): PostState {
  switch (action.type) {
    case postConstants.GET_POST:
      return {
        ...state,
        loaded: false,
      }

    case postConstants.GET_POST_SUCCESS:
      return {
        ...state,
        loaded: true,
        item: action.payload,
      }

    case postConstants.GET_POST_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
