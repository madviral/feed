export const textSlice = (text: string, length: number) => {
  if (text.length <= length) {
    return text
  } else {
    return `${text.slice(0, length)}...`
  }
}

export const scrollToTop = (top: number, left: number) => {
  window.scrollTo({
    top,
    left,
    behavior: 'smooth',
  })
}
