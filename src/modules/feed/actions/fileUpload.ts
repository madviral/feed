import { ActionCreator } from 'redux'
import { fileUploadConstants } from '../constants'
import { AppThunk } from 'src/store/rootReducer'

export const fileUpload = (file: File): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  const fileData = new FormData()
  fileData.append('file', file, file.name)
  const { error, data } = await client.post(
    `entertainment/feed/v1/file/upload`,
    fileData,
  )
  if (!error) {
    dispatch(fileUploadSucceeded(data?.fileUrl))
  } else {
    alert('error')
    dispatch(fileUploadFailure(error))
  }
}

export const fileUploadSucceeded: ActionCreator<any> = (payload: string) => ({
  type: fileUploadConstants.FILE_UPLOADED_SUCCESS,
  payload,
})

export const fileUploadFailure: ActionCreator<any> = error => ({
  type: fileUploadConstants.FILE_UPLOADED_ERROR,
  error,
})
