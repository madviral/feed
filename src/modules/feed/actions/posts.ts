import { ActionCreator } from 'redux'
import { postConstants } from '../constants'
import { Posts, PostUpload } from '../types/feed'
import { AppThunk } from 'src/store/rootReducer'
import { PostItem } from 'src/common'

export const requestPosts = ({
  resource = null,
  page = 1,
  tag = null,
  category = null,
} = {}): AppThunk => async (dispatch, getState, client) => {
  dispatch(loadPosts())
  const { error, data } = await client.get(`entertainment/feed/v1/posts`, {
    params: {
      resource,
      page,
      tag,
      category,
    },
  })
  if (!error) {
    dispatch(loadPostsSucceeded(data.list))
  } else {
    alert('error')
    dispatch(loadPostsFailure(error))
  }
}

export const loadPosts: ActionCreator<any> = () => ({
  type: postConstants.GET_POSTS,
})

export const loadPostsSucceeded: ActionCreator<any> = (payload: Posts) => ({
  type: postConstants.GET_POSTS_SUCCESS,
  payload,
})

export const loadPostsFailure: ActionCreator<any> = error => ({
  type: postConstants.GET_POSTS_ERROR,
  error,
})

export const requestPost = (id: string): AppThunk => async (
  dispatch,
  getState,
  client,
) => {
  const { error, data } = await client.get(`entertainment/feed/v1/posts/${id}`)
  if (!error) {
    dispatch(loadPostSucceeded(data))
  } else {
    alert('error')
    dispatch(loadPostFailure(error))
  }
}

export const loadPost: ActionCreator<any> = () => ({
  type: postConstants.GET_POST,
})

export const loadPostSucceeded: ActionCreator<any> = (payload: PostItem) => ({
  type: postConstants.GET_POST_SUCCESS,
  payload,
})

export const loadPostFailure: ActionCreator<any> = error => ({
  type: postConstants.GET_POST_ERROR,
  error,
})
