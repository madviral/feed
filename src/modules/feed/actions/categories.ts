import { ActionCreator } from 'redux'
import { categoriesConstants } from '../constants'

export const requestCategories: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await client.get('entertainment/feed/v1/categories')
  if (!error) {
    dispatch(loadCategoriesSucceeded(data))
  } else {
    alert('error')
    dispatch(loadCategoriesFailure(error))
  }
}

export const loadCategories: ActionCreator<any> = () => ({
  type: categoriesConstants.GET_CATEGORIES,
})

export const loadCategoriesSucceeded: ActionCreator<any> = payload => ({
  type: categoriesConstants.GET_CATEGORIES_SUCCESS,
  payload,
})

export const loadCategoriesFailure: ActionCreator<any> = error => ({
  type: categoriesConstants.GET_CATEGORIES_ERROR,
  error,
})
