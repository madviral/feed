import { ActionCreator } from 'redux'
import { filterConstants } from '../constants'
import { ResourceItem } from 'src/common'

export const requestResources: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await client.get('entertainment/feed/v1/resources')
  if (!error) {
    dispatch(loadResources(data))
  } else {
    alert(error)
  }
}

export const loadResources: ActionCreator<any> = (payload: ResourceItem[]) => ({
  type: filterConstants.GET_RESOURCES,
  payload,
})

export const saveResource: ActionCreator<any> = (payload: string) => ({
  type: filterConstants.SAVE_RESOURCE,
  payload,
})

export const deleteResource: ActionCreator<any> = () => ({
  type: filterConstants.DELETE_RESOURCE,
})
