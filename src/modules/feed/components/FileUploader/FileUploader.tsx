import React, { FC } from 'react'
import { styled } from 'ui'
import { FileUploader } from '../../types/feed'

const FormFileUploader = styled.div`
  width: fit-content;
  position: relative;

  input {
    position: absolute;
    top: 0;
    left: 0;
    visibility: hidden;
    opacity: 0;
  }
`

const FileUploader: FC<FileUploader> = ({ icon, onFileUpload }) => {
  return (
    <FormFileUploader>
      <form>
        <input
          id='1'
          type='file'
          accept='.jpg, .jpeg, .png .gif'
          onChange={event => onFileUpload(event.target.files[0])}
        />
        <label htmlFor='1'>{icon}</label>
      </form>
    </FormFileUploader>
  )
}

export default FileUploader
