import React, { FC } from 'react'
import { Link } from 'react-router-dom'
import { Condition } from '@vlife-grand-era/vlife-storybook'
import { styled, Box, Text, theme } from 'ui'
import { CardContent } from '..'
import { PostFeature } from 'src/components'
import { PostItem } from 'src/common'
import { CardProps } from '../types'

const TagWrapper = styled.div`
  padding-left: 8px;
  border-left: 1px solid #757575;
`

const TagInnerWrapper = styled.div`
  display: flex;
  flex-flow: row wrap;
  transform: translateY(2px);
`

const Card: FC<PostItem & CardProps> = ({
  id,
  userId,
  firstName,
  lastName,
  title,
  body,
  url,
  image,
  resource,
  tags,
  resourceType,
  likesCount,
  disLikesCount,
  thumbState,
  onTagClick,
}) => (
  <Box
    px={16}
    pt={16}
    pb={32}
    mb='2px'
    backgroundColor={theme.colorList.secondary[9]}
    borderRadius={theme.borderRadius.m}
    boxShadow='0px 0px 16px rgba(0, 0, 0, 0.05), 0px 0px 4px rgba(0, 0, 0, 0.1)'
  >
    <Condition match={userId}>
      <Link to={`/profile/${userId}`} style={{ textDecoration: 'none' }}>
        <Text
          as='p'
          mb={16}
          fontWeight={theme.fontWeight.medium}
          fontSize={12}
          lineHeight='14px'
          color={theme.colorList.secondary[0]}
        >
          {firstName} {lastName}
        </Text>
      </Link>
    </Condition>

    {resourceType === 'external' && (
      <a href={url} style={{ textDecoration: 'none' }}>
        <CardContent image={image} title={title} body={body} />
      </a>
    )}
    {resourceType === 'internal' && (
      <Link to={`/post/${id}`} style={{ textDecoration: 'none' }}>
        <CardContent image={image} title={title} body={body} />
      </Link>
    )}
    <Box display='flex'>
      <Text
        mr='8px'
        fontSize={12}
        lineHeight='14px'
        color={theme.colorList.secondary[3]}
        style={{ transform: 'translateY(6px)' }}
      >
        {resource.toLowerCase()}
      </Text>
      {tags !== null && (
        <TagWrapper>
          <TagInnerWrapper>
            {tags.map((tag: string) => (
              <Box
                key={tag}
                p='4px'
                mb='4px'
                mr='4px'
                backgroundColor={theme.colorList.primary[9]}
                borderRadius={theme.borderRadius.xxs}
                onClick={() => onTagClick(tag)}
              >
                <Text
                  as='p'
                  fontWeight={theme.fontWeight.medium}
                  fontSize={12}
                  lineHeight='14px'
                  color={theme.colorList.primary[5]}
                >
                  {tag}
                </Text>
              </Box>
            ))}
          </TagInnerWrapper>
        </TagWrapper>
      )}
    </Box>
    {resourceType === 'internal' && (
      <Box mt={16}>
        <PostFeature
          postId={id}
          likes={likesCount}
          disLikes={disLikesCount}
          thumbState={thumbState}
        />
      </Box>
    )}
  </Box>
)

export default Card
