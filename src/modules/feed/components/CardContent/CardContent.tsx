import React from 'react'
import { styled, Box, Text, theme } from 'ui'
import { textSlice } from '../../utils'

const Image = styled.img`
  display: block;
  width: 100%;
`

const CardContent = ({ title, body, image }) => (
  <>
    {image && (
      <Box mb='8px' borderRadius={theme.borderRadius.xs} overflow='hidden'>
        <Image src={image} alt='' />
      </Box>
    )}
    <Text
      as='p'
      mb='4px'
      fontWeight={theme.fontWeight.semibold}
      fontSize={16}
      lineHeight='20px'
      color={theme.colorList.secondary[0]}
    >
      {title}
    </Text>
    <Text
      as='p'
      mb={16}
      fontSize={12}
      lineHeight='16px'
      color={theme.colorList.secondary[0]}
      style={{ overflow: 'hidden' }}
    >
      {textSlice(body, 110)}
    </Text>
  </>
)

export default CardContent
