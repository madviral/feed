import React, { FC } from 'react'
import { Box, theme, Text, CrossRoundedIcon, styled, RadioButton } from 'ui'
import { CategoryListProps } from '../..'
import { CategoryItem } from 'src/common'

const Button = styled.button`
  display: flex;
  background-color: transparent;
  padding: 0;
  border: none;
  outline: none;
`

const CategoryList: FC<CategoryListProps> = ({
  items,
  selectedItem,
  onCloseClick,
  onItemClick,
}) => (
  <Box p={16} backgroundColor={theme.colorList.secondary[8]}>
    <Box
      display='flex'
      alignItems='center'
      justifyContent='space-between'
      mb={16}
    >
      <Text
        as='p'
        fontWeight={theme.fontWeight.semibold}
        fontSize={20}
        lineHeight='24px'
        color={theme.colorList.secondary[1]}
      >
        Выберите категорию
      </Text>
      <Button onClick={onCloseClick}>
        <CrossRoundedIcon />
      </Button>
    </Box>
    <Box borderRadius='8px 8px 0px 0px'>
      {items.map((item: CategoryItem) => (
        <RadioButton
          key={item.id}
          id={item.id.toString()}
          value={item.id}
          name='categories'
          label={item.name}
          checked={selectedItem === item.id.toString()}
          onChange={(value: string) => onItemClick(value)}
        />
      ))}
    </Box>
  </Box>
)

export default CategoryList
