export interface CardProps {
  onTagClick: (tagName: string) => void
}
