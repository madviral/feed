import React, { useEffect, FC } from 'react'
import { connect } from 'react-redux'
import { Dispatch, bindActionCreators } from 'redux'
import { useParams, Link } from 'react-router-dom'
import { styled, Box, Text, theme } from 'ui'
import { Container, Header, PostFeature } from 'src/components'
import * as postsActions from '../../actions/posts'
import { PostDetailProps } from '../../types'

const Image = styled.img`
  display: block;
  width: 100%;
`

const PostDetail: FC<PostDetailProps> = ({ post, requestPost }) => {
  const { postId } = useParams()

  useEffect(() => {
    requestPost(postId)
  }, [])

  return (
    <Container bgColor={theme.colorList.secondary[8]}>
      <Header
        title='Билл Гейтс о романе Нила Стивенсона «Семиевие»: почему его нужно читать каждому'
        displayRight={false}
        fontWeight={theme.fontWeight.semibold}
        fontSize='16px'
        color={theme.colorList.secondary[1]}
        onBackButtonClick={() => history.back()}
      />
      <Box p={16}>
        <Text
          as='p'
          mb={16}
          fontWeight={theme.fontWeight.medium}
          fontSize={12}
          lineHeight='14px'
          color={theme.colorList.secondary[0]}
        >
          {post.item?.firstName} {post.item?.lastName}
        </Text>
        {post.item?.image && (
          <Box mb='8px' borderRadius={theme.borderRadius.xs} overflow='hidden'>
            <Image src={post.item?.image} alt='' />
          </Box>
        )}
        <Text
          as='p'
          mb={16}
          fontSize={12}
          lineHeight='16px'
          color={theme.colorList.secondary[0]}
          style={{ overflow: 'hidden' }}
        >
          {post.item?.body}
        </Text>
        {post.item?.resourceType === 'internal' && (
          <Box mt={16}>
            <PostFeature
              postId={post.item?.id}
              likes={post.item?.likesCount}
              disLikes={post.item?.disLikesCount}
              thumbState={post.item?.thumbState}
            />
          </Box>
        )}
      </Box>
    </Container>
  )
}
const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(postsActions, dispatch),
})

const mapStateToProps = state => ({
  post: state.post,
})

export default connect(mapStateToProps, mapDispatchToProps)(PostDetail)
