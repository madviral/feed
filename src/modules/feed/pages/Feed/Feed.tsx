import React, { FC, useEffect, useState } from 'react'
import { RouteComponentProps } from 'react-router-dom'
import { connect } from 'react-redux'
import { bindActionCreators, Dispatch } from 'redux'
import { Drawer } from '@material-ui/core'
import * as postsActions from '../../actions/posts'
import * as resourceActions from '../../actions/resources'
import * as categoriesActions from '../../actions/categories'
import {
  Text,
  theme,
  Box,
  RefreshIcon,
  CrossIcon,
  QuestionMark,
  SmoothSlider,
  PencilIcon,
  Button,
  CategoryIcon,
} from 'ui'
import { Card, CategoryList } from '../../components'
import { Header, Spinner, ErrorHandler, Container } from 'src/components'
import { scrollToTop } from '../../utils'
import { PostsActions } from '../../types'
import { Queries } from '../../types/enums'
import { withAuth, PostItem } from 'src/common'

const Feed: FC<PostsActions & RouteComponentProps> = ({
  authObj,
  posts,
  resources,
  location,
  history,
  categories,
  requestPosts,
  requestResources,
  requestCategories,
}) => {
  const [pageNumber, setPageNumber] = useState(1)
  const [isCategoryDrawerOpen, setIsCategoryDrawerOpen] = useState(false)
  const [isAuthDrawerOpen, setIsAuthDrawerOpen] = useState(false)

  const query = new URLSearchParams(location.search)

  useEffect(() => {
    requestResources()
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag: query.get(`${Queries.TAG}`),
      page: pageNumber,
      category: query.get(`${Queries.CATEGORY}`),
    })
  }, [])

  const refreshPage = () => {
    setPageNumber(pageNumber + 1)
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag: query.get(`${Queries.TAG}`),
      page: pageNumber + 1,
      category: query.get(`${Queries.CATEGORY}`),
    })
    scrollToTop(0, 0)
  }

  const onTagClick = (tag: string) => {
    setPageNumber(1)
    query.set(Queries.TAG, tag)
    history.replace({ ...history.location, search: query.toString() })
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag,
      page: 1,
      category: query.get(`${Queries.CATEGORY}`),
    })
    scrollToTop(0, 0)
  }

  const handleSearchSubmit = (value: string) => {
    setPageNumber(1)
    query.set(Queries.TAG, value)
    history.replace({ ...history.location, search: query.toString() })
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag: value,
      page: 1,
      category: query.get(`${Queries.CATEGORY}`),
    })
  }

  const handleSlideItemClick = (value: string) => {
    setPageNumber(1)
    query.set(Queries.RESOURCE, value)
    history.replace({ ...history.location, search: query.toString() })
    requestPosts({
      resource: value,
      tag: query.get(`${Queries.TAG}`),
      page: 1,
      category: query.get(`${Queries.CATEGORY}`),
    })
  }

  const handleTagClose = () => {
    query.delete(Queries.TAG)
    history.replace({ ...history.location, search: query.toString() })
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag: query.get(`${Queries.TAG}`),
      page: pageNumber,
      category: query.get(`${Queries.CATEGORY}`),
    })
  }

  const handleCategoryChange = (value: string) => {
    setPageNumber(1)
    query.set(Queries.CATEGORY, value)
    history.replace({ ...history.location, search: query.toString() })
    requestPosts({
      resource: query.get(`${Queries.RESOURCE}`),
      tag: query.get(`${Queries.TAG}`),
      page: 1,
      category: query.get(`${Queries.CATEGORY}`),
    })
  }

  const onBackButtonClick = () => {
    if (typeof Android !== 'undefined') {
      Android.closeWebView()
    }
  }

  return (
    <>
      <Container bgColor={theme.colorList.secondary[8]}>
        <Box display='flex' flexDirection='column' minHeight='100vh'>
          <Header
            withPhoto
            title='V лента'
            onBackButtonClick={onBackButtonClick}
            onSearchTextChange={handleSearchSubmit}
            onProfileClick={() => {
              withAuth(history, '/profile', authObj.isAuthenticated).then(
                value => {
                  if (!value) {
                    return value
                  } else {
                    setIsAuthDrawerOpen(true)
                  }
                },
              )
            }}
          />
          <Box
            display='flex'
            alignItems='center'
            justifyContent='space-between'
            mt={16}
            p='8px'
          >
            <Button
              text='Категорий'
              pd='8px'
              borderRadius='0'
              bgColor='transparent'
              fontSize='16px'
              lineHeight='18px'
              fontWeight={500}
              color={theme.colorList.secondary[0]}
              icon={<CategoryIcon />}
              onClick={() => {
                requestCategories()
                setIsCategoryDrawerOpen(true)
              }}
            />
            <Box mr='8px'>
              <QuestionMark />
            </Box>
          </Box>
          {resources.loaded && !posts.error && (
            <Box py={16} px='8px'>
              <SmoothSlider
                list={resources.list}
                name='resources'
                onSelect={handleSlideItemClick}
                selectedItem={query.get(`${Queries.RESOURCE}`)}
              />
            </Box>
          )}
          {query.get(`${Queries.TAG}`) !== null && (
            <Box px='8px' mb={16}>
              <Box
                display='flex'
                width='fit-content'
                p='4px'
                backgroundColor={theme.colorList.primary[9]}
                borderRadius={theme.borderRadius.xxs}
              >
                <Text
                  mr='4px'
                  fontSize='14px'
                  lineHeight='18px'
                  fontWeight={theme.fontWeight.medium}
                  color={theme.colorList.primary[5]}
                >
                  {query.get(`${Queries.TAG}`)}
                </Text>
                <Box onClick={handleTagClose}>
                  <CrossIcon />
                </Box>
              </Box>
            </Box>
          )}
          {posts.loaded && !posts.error && (
            <>
              {posts.list.map((post: PostItem) => (
                <Card
                  key={post.id}
                  userId={post.userId}
                  id={post.id}
                  firstName={post.firstName}
                  lastName={post.lastName}
                  title={post.title}
                  body={post.body}
                  url={post.url}
                  image={post.image}
                  resource={post.resource}
                  tags={post.tags}
                  resourceType={post.resourceType}
                  likesCount={post.likesCount}
                  disLikesCount={post.disLikesCount}
                  thumbState={post.thumbState}
                  onTagClick={onTagClick}
                />
              ))}
               
              {posts.list.length >= 20 && (
                <Box
                  display='flex'
                  alignItems='center'
                  justifyContent='center'
                  width={220}
                  py={18}
                  mx='auto'
                  onClick={() => refreshPage()}
                >
                  <RefreshIcon />
                  <Text
                    ml={16}
                    fontWeight={theme.fontWeight.medium}
                    fontSize={16}
                    lineHeight='20px'
                    color={theme.colorList.secondary[5]}
                  >
                    Загрузить еще
                  </Text>
                </Box>
              )}
            </>
          )}
          {!posts.loaded && !posts.error && (
            <Box
              display='flex'
              alignItems='center'
              justifyContent='center'
              flex='1'
            >
              <Spinner />
            </Box>
          )}
          {posts.error && (
            <Box pt={16} px={16}>
              <ErrorHandler />
            </Box>
          )}
        </Box>
      </Container>
      <button
        onClick={() =>
          withAuth(history, '/create?step=first', authObj.isAuthenticated).then(
            value => {
              if (!value) {
                return value
              } else {
                setIsAuthDrawerOpen(true)
              }
            },
          )
        }
        style={{
          position: 'fixed',
          right: '8px',
          bottom: '30px',
          display: 'flex',
          alignItems: 'center',
          justifyContent: 'center',
          width: '56px',
          height: '56px',
          backgroundColor: `${theme.colorList.primary[5]}`,
          borderRadius: `${theme.borderRadius.r}`,
          outline: 'none',
          border: 'none',
        }}
      >
        <PencilIcon />
      </button>
      <Drawer
        classes={{ paper: 'custom_drawer' }}
        anchor='bottom'
        open={isCategoryDrawerOpen}
        onClose={() => setIsCategoryDrawerOpen(false)}
      >
        <CategoryList
          items={categories.list}
          selectedItem={query.get(`${Queries.CATEGORY}`)}
          onCloseClick={() => setIsCategoryDrawerOpen(false)}
          onItemClick={handleCategoryChange}
        />
      </Drawer>
      <Drawer
        classes={{ paper: 'custom_drawer' }}
        anchor='bottom'
        open={isAuthDrawerOpen}
        onClose={() => setIsAuthDrawerOpen(false)}
      >
        <ErrorHandler
          title='Нужно войти в систему'
          text='Авторизуйтесь, чтобы вам было удобнее пользоваться VLIFE.'
          buttonText='Авторизоваться'
          onButtonClick={() => {
            Android.openAuth()
            setIsAuthDrawerOpen(false)
          }}
        />
      </Drawer>
    </>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(postsActions, dispatch),
  ...bindActionCreators(resourceActions, dispatch),
  ...bindActionCreators(categoriesActions, dispatch),
})

const mapStateToProps = state => ({
  authObj: state.auth,
  posts: state.posts,
  resources: state.resources,
  categories: state.categories,
})

export default connect(mapStateToProps, mapDispatchToProps)(Feed)
