import { ReactNode } from 'react'
import { TokenObj, PostItem, ResourceItem, CategoryItem } from 'src/common'

export interface Posts {
  list: PostItem[]
  pageNumber: number
  total: number
  totalPages: number
}

export interface PostsActions {
  authObj: TokenObj
  posts: { loaded: boolean; list: PostItem[]; error: any }
  resources: { loaded: boolean; list: ResourceItem[] }
  categories: { loaded: boolean; list: CategoryItem[] }
  requestResources: () => void
  saveResource: (items: string) => void
  requestPosts: ({}: {
    resource?: string
    tag?: string
    page?: number
    category?: string
  }) => void
  requestCategories: () => void
}

export interface FileUploader {
  icon: ReactNode
  onFileUpload: (file: File) => void
}

export interface PostUpload {
  body: string
  categoryName: string
  imageUrl: string
  tag: string[]
  title: string
}

export interface CategoryListProps {
  items: CategoryItem[]
  selectedItem?: string
  onCloseClick?: () => void
  onItemClick?: (value?: string) => void
}

export interface PostDetailProps {
  post: { item: PostItem }
  requestPost: (id: string) => void
}
