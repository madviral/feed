export enum Queries {
  RESOURCE = 'resource',
  TAG = 'tag',
  STEP = 'step',
  CATEGORY = 'category',
}
