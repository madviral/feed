import { profilePostConstants } from '../constants'
import { ProfilePost } from '../types/profile'

export interface ProfilePostState {
  loaded: boolean
  list: ProfilePost[]
  error?: null
}

const initialState: ProfilePostState = {
  loaded: false,
  list: [],
}

export function profilePosts(state = initialState, action): ProfilePostState {
  switch (action.type) {
    case profilePostConstants.GET_PROFILE_POSTS:
      return {
        ...state,
        loaded: false,
      }

    case profilePostConstants.GET_PROFILE_POSTS_SUCCESS:
      return {
        ...state,
        loaded: true,
        list: action.payload,
      }

    case profilePostConstants.GET_PROFILE_POSTS_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
