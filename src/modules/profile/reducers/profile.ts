import { profileConstants } from '../constants'
import { Profile } from '../types/profile'

export interface ProfileState {
  loaded: boolean
  user: Profile
  error?: null
}

const initialState: ProfileState = {
  loaded: false,
  user: null,
}

export function profile(state = initialState, action): ProfileState {
  switch (action.type) {
    case profileConstants.GET_PROFILE:
      return {
        ...state,
        loaded: false,
      }

    case profileConstants.GET_PROFILE_SUCCESS:
      return {
        ...state,
        loaded: true,
        user: action.payload,
      }

    case profileConstants.GET_PROFILE_ERROR:
      return {
        ...state,
        error: action.error,
      }

    default:
      return state
  }
}
