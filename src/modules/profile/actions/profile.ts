import { ActionCreator } from 'redux'
import { profileConstants } from '../constants'
import { Profile } from '../types/profile'

export const requestProfile: ActionCreator<any> = (userId: string) => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await (userId
    ? client.get(`entertainment/feed/v1/profile/${userId}`)
    : client.get(`entertainment/feed/v1/profile`))
  if (!error) {
    dispatch(loadProfileSucceeded(data))
  } else {
    alert('error')
    dispatch(loadProfileFailure(error))
  }
}

export const loadProfileSucceeded: ActionCreator<any> = (payload: Profile) => ({
  type: profileConstants.GET_PROFILE_SUCCESS,
  payload,
})

export const loadProfileFailure: ActionCreator<any> = error => ({
  type: profileConstants.GET_PROFILE_ERROR,
  error,
})
