import { ActionCreator } from 'redux'
import { profilePostConstants } from '../constants'
import { ProfilePost } from '../types/profile'

export const requestProfilePosts: ActionCreator<any> = () => async (
  dispatch,
  getState,
  client,
) => {
  const { data, error } = await client.get(
    'entertainment/feed/v1/posts/profile',
  )
  if (!error) {
    dispatch(loadProfilePostsSucceeded(data?.postResponseList))
  } else {
    alert('error')
    dispatch(loadProfilePostsFailure(error))
  }
}

export const loadProfilePostsSucceeded: ActionCreator<any> = (
  payload: ProfilePost,
) => ({
  type: profilePostConstants.GET_PROFILE_POSTS_SUCCESS,
  payload,
})

export const loadProfilePostsFailure: ActionCreator<any> = error => ({
  type: profilePostConstants.GET_PROFILE_POSTS_ERROR,
  error,
})
