import React, { FC, useEffect } from 'react'
import { bindActionCreators, Dispatch } from 'redux'
import { connect } from 'react-redux'
import { Box, Text, theme, BonusIcon, HumanIcon } from 'ui'
import {
  ProfileImage,
  ProfileContent,
  ProfileContentItem,
  ProfilePostItem,
} from '../../components'
import { Header, Container } from 'src/components'
import * as profileActions from '../../actions/profile'
import * as profilePostsActions from '../../actions/profilePosts'
import { ProfilePost, ProfileProps } from '../../types/profile'
import { history } from 'src/common/history'
import { onNativeBackPressed } from 'src/common/utils'
import { useParams } from 'react-router-dom'

const Profile: FC<ProfileProps> = ({
  profile,
  posts,
  requestProfile,
  requestProfilePosts,
}) => {
  const { userId } = useParams()

  useEffect(() => {
    requestProfile(userId)
    requestProfilePosts()
    if (window) {
      window.onBackPressed = onBackPressed
    }
  }, [])

  const onBackPressed = () => onNativeBackPressed(history)

  return (
    <Container bgColor={theme.colorList.secondary[9]}>
      <Box mb={16}>
        <Header onBackButtonClick={() => history.goBack()} />
        <Box
          mt={-56}
          display='flex'
          flexDirection='column'
          alignItems='center'
          justifyContent='center'
        >
          <ProfileImage url='https://i.ibb.co/tDX9qd9/Profile.jpg' />
          <Text
            as='p'
            mt='10px'
            fontWeight={theme.fontWeight.bold}
            fontSize='20px'
            lineHeight='24px'
            letterSpacing='-0.24px'
            color={theme.colorList.secondary[0]}
          >
            {profile.user?.firstName} {profile.user?.lastName}
          </Text>
        </Box>
      </Box>
      <ProfileContent>
        <Box
          display='flex'
          alignItems='center'
          justifyContent='space-between'
          py={20}
          px={16}
          gridArea='bonus'
          backgroundColor={theme.colorList.secondary[8]}
          borderRadius='8px 8px 0px 0px'
        >
          <Text
            as='p'
            fontSize='16px'
            lineHeight='18px'
            letterSpacing='-0.24px'
            color={theme.colorList.secondary[0]}
          >
            Мои бонусы
          </Text>
          <Box display='flex' alignItems='center'>
            <Text
              mr='6px'
              fontWeight={theme.fontWeight.semibold}
              fontSize='20px'
              lineHeight='24px'
              letterSpacing='-0.24px'
              color={theme.colorList.fern[3]}
            >
              {profile.user?.bonusesAmount}
            </Text>
            <BonusIcon />
          </Box>
        </Box>
        <ProfileContentItem
          gridArea='post'
          amount={profile.user?.posts}
          text='публикации'
        />
        <ProfileContentItem
          gridArea='subscriber'
          amount={profile.user?.subscribers}
          text='подписчиков'
        />
        <ProfileContentItem
          gridArea='subscription'
          amount={profile.user?.subscriptions}
          text='подписок'
        />
        <ProfileContentItem
          gridArea='vlike'
          amount={profile.user?.vlike}
          text='Vлайк'
          borderRadius='0 0 0 8px'
        />
        <ProfileContentItem
          gridArea='like'
          amount={profile.user?.likes}
          text='лайки'
        />
        <ProfileContentItem
          gridArea='dislike'
          amount={profile.user?.disLikes}
          text='дизлайки'
          borderRadius='0 0 8px 0'
        />
      </ProfileContent>
      <Box display='flex' flexDirection='column' p={16} flex={1}>
        <Text
          as='p'
          mb={16}
          fontWeight={theme.fontWeight.medium}
          fontSize='16px'
          lineHeight='20px'
          color={theme.colorList.secondary[0]}
        >
          Мои публикации
        </Text>
        {posts.list.length > 0 &&
          posts.list.map((post: ProfilePost) => (
            <ProfilePostItem
              key={post.id}
              id={post.id}
              title={post.title}
              tags={post.tags}
              image={post.image}
              likes={post.likes}
              disLikes={post.disLikes}
              thumbState={post.thumbState}
            />
          ))}
        {posts.list.length === 0 && (
          <Box
            display='flex'
            flexDirection='column'
            flex={1}
            alignItems='center'
            justifyContent='center'
          >
            <HumanIcon />
            <Text
              as='p'
              mt={16}
              fontSize='16px'
              lineHeight='20px'
              color={theme.colorList.secondary[3]}
            >
              Нет публикации
            </Text>
          </Box>
        )}
      </Box>
    </Container>
  )
}

const mapDispatchToProps = (dispatch: Dispatch) => ({
  ...bindActionCreators(profileActions, dispatch),
  ...bindActionCreators(profilePostsActions, dispatch),
})

const mapStateToProps = state => ({
  profile: state.profile,
  posts: state.profilePosts,
})

export default connect(mapStateToProps, mapDispatchToProps)(Profile)
