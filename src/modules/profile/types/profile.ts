export interface ProfileProps {
  profile: { loaded: boolean; user: Profile }
  posts: { loaded: boolean; list: ProfilePost[] }
  requestProfile: (id?: string) => void
  requestProfilePosts: () => void
}

export interface Profile {
  id: number
  birthDate: string
  characterId: null
  createdAt: string
  email: string
  emailVerified: boolean
  firstName: string
  lastName: string
  keycloakId: string
  nationalIdentifier: string
  photoUrl: string
  region: string
  sex: boolean
  updatedAt: string
  subscribers: number
  subscriptions: number
  posts: number
  likes: number
  disLikes: number
  bonusesAmount: number
  vlike: number
}

export interface ProfileContentItemProps {
  amount: number
  text: string
  gridArea: string
  borderRadius?: string
}

export interface ProfilePostItemProps {
  id: number
  title: string
  tags?: string[]
  image?: string
  likes: number
  disLikes: number
  thumbState?: number
}

export interface ProfilePost {
  id: number
  userId: string
  firstName: string
  lastName: string
  title: string
  body: string
  url: null | string
  image: null | string
  resource: string
  icon: string
  tags: string[]
  locale: string
  dateCreated: string
  dateUpdated: null
  resourceType: string
  category: string
  likes: number
  disLikes: number
  thumbState?: number
}
