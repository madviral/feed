import React, { FC } from 'react'
import { styled, theme, Text } from 'ui'
import { ProfileContentItemProps } from '../../types/profile'

interface ItemProps {
  gridArea: string
  borderRadius: string
}

const Item = styled.div<ItemProps>`
  display: flex;
  flex-direction: column;
  padding: 10px 16px;
  border-radius: ${props => props.borderRadius};
  background-color: ${props => props.theme.colorList.secondary[8]};
  grid-area: ${props => props.gridArea};
`

const ProfileContentItem: FC<ProfileContentItemProps> = ({
  amount,
  text,
  gridArea,
  borderRadius,
}) => (
  <Item gridArea={gridArea} borderRadius={borderRadius}>
    <Text
      fontWeight={theme.fontWeight.medium}
      fontSize='18px'
      lineHeight='20px'
      color={theme.colorList.secondary[0]}
    >
      {amount || 0}
    </Text>
    <Text
      as='p'
      mt='4px'
      fontSize='12px'
      lineHeight='16px'
      color={theme.colorList.secondary[3]}
    >
      {text}
    </Text>
  </Item>
)

export default ProfileContentItem
