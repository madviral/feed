import React, { FC } from 'react'
import { styled, Box, theme, Text, ImageIcon } from 'ui'
import { ProfilePostItemProps } from '../../types/profile'
import { PostFeature } from 'src/components'

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  padding: 16px;
  border-radius: ${props => props.theme.borderRadius.m};
  box-shadow: 0px 0px 16px rgba(0, 0, 0, 0.05), 0px 0px 4px rgba(0, 0, 0, 0.1);

  &:not(:last-child) {
    margin-bottom: 2px;
  }
`

const PostImage = styled.img`
  display: block;
  margin-right: 16px;
  width: 80px;
  height: 80px;
  object-fit: cover;
  background-color: ${props => props.theme.colorList.secondary[8]};
  border-radius: ${props => props.theme.borderRadius.xs};
`

const TagItem = styled.span`
  display: block;
  padding: 4px;
  font-weight: ${props => props.theme.fontWeight.medium};
  font-size: 12px;
  line-height: 14px;
  color: ${props => props.theme.colorList.primary[5]};
  background-color: ${props => props.theme.colorList.primary[9]};
  border-radius: ${props => props.theme.borderRadius.xxs};

  &:not(:last-child) {
    margin-right: 4px;
  }
`

const ImgSkeleton = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  margin-right: 16px;
  height: 80px;
  min-width: 80px;
  background-color: ${props => props.theme.colorList.secondary[8]};
  border-radius: ${props => props.theme.borderRadius.xs};
`

const ProfilePostItem: FC<ProfilePostItemProps> = ({
  id,
  title,
  tags,
  image,
  likes,
  disLikes,
  thumbState,
}) => (
  <Wrapper>
    <Box display='flex' mb={16}>
      {image !== null ? (
        <PostImage src={image} alt='' />
      ) : (
        <ImgSkeleton>
          <ImageIcon />
        </ImgSkeleton>
      )}
      <Box>
        <Text
          as='p'
          fontWeight={theme.fontWeight.semibold}
          fontSize='16px'
          lineHeight='20px'
          color={theme.colorList.secondary[0]}
        >
          {title}
        </Text>
        <Box display='flex' mt='8px'>
          {tags.map((tag: string) => (
            <TagItem key={tag}>{tag}</TagItem>
          ))}
        </Box>
      </Box>
    </Box>
    <PostFeature
      postId={id}
      likes={likes}
      disLikes={disLikes}
      thumbState={thumbState}
    />
  </Wrapper>
)

export default ProfilePostItem
