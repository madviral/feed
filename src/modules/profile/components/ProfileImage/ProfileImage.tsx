import React from 'react'
import { styled } from 'ui'

const Image = styled.img`
  display: block;
`
const ProfileImage = ({ url }) => <Image src={url} alt='' />

export default ProfileImage
