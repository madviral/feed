import React from 'react'
import { styled } from 'ui'

const Wrapper = styled.div`
  display: grid;
  grid-template-areas:
    'bonus bonus bonus'
    'post subscriber subscription'
    'vlike like dislike';
  grid-template-columns: 1fr 1fr 1fr;
  grid-template-rows: auto auto;
  grid-gap: 1px;
  padding: 0 16px;
`

const ProfileContent = ({ children }) => <Wrapper>{children}</Wrapper>

export default ProfileContent
