import { combineReducers, Action } from 'redux'
import { ThunkAction } from 'redux-thunk'
import { AxiosInstance } from 'axios'
import { createdPost } from '../modules/createPost'
import { post, posts, resources, fileUpload, categories } from '../modules/feed'
import { profile, profilePosts } from '../modules/profile'
import { auth } from '../common/reducers/auth'

export const rootReducer = combineReducers({
  auth,
  post,
  posts,
  createdPost,
  resources,
  fileUpload,
  categories,
  profile,
  profilePosts,
})

export type RootState = ReturnType<typeof rootReducer>

export type AppThunk<ReturnType = void> = ThunkAction<
  ReturnType,
  RootState,
  AxiosInstance,
  Action<string>
>
