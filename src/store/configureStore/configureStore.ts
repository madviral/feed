import { createStore, Store, applyMiddleware } from 'redux'
import { composeWithDevTools } from 'redux-devtools-extension'
import thunk, { ThunkMiddleware } from 'redux-thunk'
import { rootReducer } from '../rootReducer'
import apiMiddleware from './middleware/api'
import { history } from '../../common/history'

export const store: Store = createStore(
  rootReducer,
  composeWithDevTools(
    applyMiddleware(apiMiddleware(history), thunk as ThunkMiddleware),
  ),
)
