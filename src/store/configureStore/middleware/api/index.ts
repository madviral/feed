import axios, { AxiosInstance } from 'axios'
import AxiosInterceptors from './interceptors'
import { requestAuth } from 'src/common/actions/auth'

export default history => ({ dispatch, getState }) => {
  const client: AxiosInstance = axios.create({
    baseURL: process.env.BASE_URL,
    headers: {
      // common: {
      //   'Accept-Language': i18n.language,
      // },
    },
  })

  const axiosInterceptors = new AxiosInterceptors(client)

  client.interceptors.request.use(
    axiosInterceptors.getRequestSuccess(getState),
    axiosInterceptors.getRequestError(),
  )

  client.interceptors.response.use(
    axiosInterceptors.getResponseSuccess(),
    axiosInterceptors.getResponseError(getState, token =>
      dispatch(requestAuth(token)),
    ),
  )

  return next => action => {
    if (typeof action === 'function') {
      action(dispatch, getState, client, history)
    } else {
      next(action)
    }
  }
}
